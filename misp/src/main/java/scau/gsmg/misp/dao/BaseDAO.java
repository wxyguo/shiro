package scau.gsmg.misp.dao;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * @Title: BaseDAO.java
 * @Group: gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */

@Repository
public class BaseDAO {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Resource
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getSession() {
		Session session = sessionFactory.openSession();
		return session;
	}
}
