package scau.gsmg.misp.dao;

import java.util.List;

import scau.gsmg.misp.vo.Permission;

public interface IMenuDao {

	//public List<Permission> searchMenu();
	public List<Permission> searchMenu(int loginID);
}
