package scau.gsmg.misp.dao;

import java.util.List;

import scau.gsmg.misp.vo.Role;
import scau.gsmg.misp.vo.User;

/**
 * @Title IRoleDAO.java
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */
public interface IRoleDAO {
	/**
	 * @Description 分页获取所有用户角色
	 */
	public List getAllRole(String page, String rows, String sort, String order)
			throws Exception;

	/**
	 * @Description 统计一共有多少行数据
	 */
	public int getRoleCount() throws Exception;

	/**
	 * @Description 添加新角色
	 */
	public boolean addRole(Role role) throws Exception;

	/**
	 * @Description 修改角色
	 */
	public boolean editRole(Role role, int id) throws Exception;

	/**
	 * @Description 删除角色
	 */
	public boolean removeRole(int id) throws Exception;
	
	public <T> T searchRole(User user);
}
