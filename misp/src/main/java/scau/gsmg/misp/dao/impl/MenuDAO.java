package scau.gsmg.misp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import scau.gsmg.misp.dao.BaseDAO;
import scau.gsmg.misp.dao.IMenuDao;
import scau.gsmg.misp.vo.Permission;
import scau.gsmg.misp.vo.RolePermission;
import scau.gsmg.misp.vo.UserRole;

@Repository
public class MenuDAO extends BaseDAO implements IMenuDao{

	@Override
	public List<Permission> searchMenu(int loginID) {
		// TODO Auto-generated method stub
		Session session=getSession();
		List<Permission> permission=new ArrayList<Permission>();
//		List<UserRole> totle
		String hqlRole="from UserRole where user.id=?";
		Query queryRole=session.createQuery(hqlRole);
		queryRole.setParameter(0, loginID);
		List<UserRole> userRole=queryRole.list();
		if(userRole.size()!=0){
			for(UserRole ur:userRole){
				String hqlRolePer="from RolePermission where role.id=?";
				Query queryRolePer=session.createQuery(hqlRolePer);
				queryRolePer.setParameter(0, ur.getRole().getId());
				List<RolePermission> rolePer=queryRolePer.list();
				if(rolePer.size()!=0){
					for(RolePermission rp:rolePer){
						String hqlPer="from Permission where id=? and type='F' ";
						Query queryPer=session.createQuery(hqlPer);
						queryPer.setParameter(0,rp.getPermission().getId());
						List<Permission> per=queryPer.list();
						if(per.size()!=0){
							for(Permission p :per){
								if(permission.size()==0){
									if(p.getSort()!=null||!"".equals(p.getSort())){
										Permission ps=getParent(Integer.valueOf(p.getSort()));
										permission.add(ps);
									}
									permission.add(p);
								}else{
									if(!isExsit(permission,p.getId())){
										System.out.println(p.getName()+"  success");
										if(p.getSort()!=null||!"".equals(p.getSort())){
											Permission ps=getParent(Integer.valueOf(p.getSort()));
											permission.add(ps);
										}
										permission.add(p);
									}
								}
							}
						}
						
					}
				}
				
			}
		}
		
		
		/*String hql="from Permission where type='F' or type='P'";
		Query query=session.createQuery(hql);
		//query.setParameter(0, );
		permission=query.list();*/
		if(permission!=null){
			System.out.println(permission.size()+" MenuDAO");
		}
		System.out.println("+++++");
		session.close();
		return permission;
	} 
	private Permission getParent(int pid){
		Session session=getSession();
		String hql="from Permission where id=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, pid);
		List<Permission>per=query.list();
		if(per.size()!=0){
			for(Permission p:per){
				session.close();
				return p;
			}
		}
		session.close();
		return null;
	}
	private boolean isExsit(List<Permission> per,int parameter){
		for(Permission p:per){
			if(p.getId()==parameter){
				return true;
			}
		}
		return false;
	}
	


}
