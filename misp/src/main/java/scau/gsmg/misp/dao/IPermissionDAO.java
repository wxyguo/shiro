package scau.gsmg.misp.dao;

import java.util.List;

import scau.gsmg.misp.vo.Permission;
public interface IPermissionDAO {

	public List<Permission> findPermissions(Integer roleId);
}
