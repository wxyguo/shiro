package scau.gsmg.misp.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.shiro.authc.UnknownAccountException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import scau.gsmg.misp.dao.BaseDAO;
import scau.gsmg.misp.dao.IRoleDAO;
import scau.gsmg.misp.vo.Member;
import scau.gsmg.misp.vo.Role;
import scau.gsmg.misp.vo.RolePermission;
import scau.gsmg.misp.vo.User;
import scau.gsmg.misp.vo.UserRole;

/**
 * @Title 管理员对角色的所有管理操作
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */
@Repository
public class RoleDAO extends BaseDAO implements IRoleDAO {

	/**
	 * 得到所有的角色信息，并且分页
	 */
	@Override
	public List getAllRole(String page, String rows, String sort, String order)
			throws Exception {
		// TODO Auto-generated method stub
		Session session = getSession();
		String hql = "from Role r";
		hql = hql + " order by " + sort + " " + order;
		List dataList = new ArrayList();
		try {
			// 当为缺省值的时候进行赋值
			int currentpage = Integer
					.parseInt((page == null || page == "0") ? "1" : page);// 第几页
			int pagesize = Integer
					.parseInt((rows == null || rows == "0") ? "10" : rows);// 每页多少行
			Query query = session.createQuery(hql);
			List<Role> role = query
					.setFirstResult((currentpage - 1) * pagesize)
					.setMaxResults(pagesize).list();
			for (Role r : role) {
				HashMap<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("role.id", r.getId());
				jsonMap.put("role.description", r.getDescription());
				jsonMap.put("role.name", r.getName());
				jsonMap.put("role.roleCode", r.getRoleCode());
				dataList.add(jsonMap);
			}

			return dataList;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		} finally {
			session.close();
		}
		return null;
	}

	/**
	 * 统计一共有多少行数据
	 */
	@Override
	public int getRoleCount() throws Exception {
		// TODO Auto-generated method stub
		Session session = getSession();
		String hql = "from Role r";
		try {
			Query query = session.createQuery(hql);
			List role = query.list();
			int size = role.size();
			return size;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		} finally {
			session.close();
		}
		return 0;
	}

	/**
	 * 添加新角色
	 */
	@Override
	public boolean addRole(Role role) throws Exception {
		// TODO Auto-generated method stub
		boolean flag = false;
		Session session = getSession();
		try {
			Transaction tx = session.beginTransaction();
			session.save(role);
			tx.commit();
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		} finally {
			session.close();
		}
		return flag;
	}

	/**
	 * @Description 修改角色
	 */
	@Override
	public boolean editRole(Role role, int id) throws Exception {
		// TODO Auto-generated method stub
		boolean flag = false;
		Session session = getSession();
		try {
			String hql = "update Role r set r.name=?,r.roleCode=?,r.description=? where r.id=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, role.getName());
			query.setParameter(1, role.getRoleCode());
			query.setParameter(2, role.getDescription());
			query.setParameter(3, id);

			query.executeUpdate();
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			session.close();
		}
		return flag;
	}

	/**
	 * 删除角色
	 */
	@Override
	public boolean removeRole(int id) throws Exception {
		// TODO Auto-generated method stub
		boolean flag = false;
		Session session = getSession();
		try {
			String hql = "delete Role r where r.id=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			query.executeUpdate();
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			session.close();
		}
		return flag;
	}

	@Override
	public List<Role> searchRole(User user) {
		// TODO Auto-generated method stub
		String hql = "";
		Session session = getSession();
		User success=new User();
		success=null;
		List<Role> role=new ArrayList<Role>();

		hql = "from User where id=?";
		//try {
			Query query = session.createQuery(hql);
			System.out.println(user.getName() + user.getPassword());
			query.setParameter(0, user.getId());
			List<User> list = query.list();
		
			if (list.size() != 0) {
				System.out.println(list.size());
				success=list.get(0);
				
				for(UserRole ur:success.getUserRoles()){
					role.add(ur.getRole());
					System.out.println(ur.getRole().getRoleCode()+"  "+ur.getRole().getName()+"   "+ur.getId());
				}
				System.out.println("role.size"+role.size());
				session.close();
				return role;
			}
			System.out.println("UUUUUUUU");
		/*} catch (Exception e) {
			System.out.println("捕获异常：ww" + e);
			// return 0;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}*/
		session.close();
		return null;
	}

}
