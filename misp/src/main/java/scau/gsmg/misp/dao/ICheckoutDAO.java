package scau.gsmg.misp.dao;

import java.util.List;

import scau.gsmg.misp.vo.Member;
import scau.gsmg.misp.vo.SaleOrder;
import scau.gsmg.misp.vo.User;

/**
 * @Title 收银的所有操作
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月9日
 */
public interface ICheckoutDAO {
	/**
	 * @Description 收银员通过商品条码搜索商品
	 */
	public List searchGoodsById(String barcode) throws Exception;

	/**
	 * @Description 得到销售单最新一条记录
	 */
	public String lastSaleRecord() throws Exception;

	/**
	 * @Description 得到支付单最新一条记录
	 */
	public String lastPaymentRecord() throws Exception;

	/**
	 * @Description 提取单据
	 */
	public List gainSuspend() throws Exception;

	/**
	 * @Description 提取单据——获取单据中的商品
	 */
	public List gainSuspendGoods(int suspendId) throws Exception;

	/**
	 * @Description 提取单据——提取单据成功后删除挂账信息
	 */
	public boolean deleteRecord(int suspendId) throws Exception;

	/**
	 * @Description 结算——通过收银员id返回收银员对象
	 */
	public User gainCashier(int cashierId) throws Exception;

	/**
	 * @Description 结算——通过会员id返回用户对象
	 */
	public Member gainCustomer(int customerId) throws Exception;

	/**
	 * @Description 更新会员积分
	 */
	public boolean updateIntegral(int customerId, double totalPrice)
			throws Exception;

	/**
	 * @Description 销售开单——在销售单生成销售记录
	 */
	public SaleOrder createSaleOrder(String saleId, Member customer,
			Double totalPrice, int amount, double receivables) throws Exception;

	/**
	 * @Description 销售开单——把客户购买的商品信息写入商品销售表
	 */
	public boolean writeGoodsItem(String rowObj, SaleOrder saleOrder)
			throws Exception;

	/**
	 * @Description 结算——通过销售单号返回销售单对象
	 */
	public SaleOrder gainSaleOrder(String saleId) throws Exception;

	/**
	 * @Description 结算
	 */
	public boolean checkout(SaleOrder saleOrder, User cashier,
			String paymentId, Double totalPrice, Double realPay, Double change,
			String paymentMethod) throws Exception;

	/**
	 * @Description 结算——把销售单标记为“已支付”或“挂账”，0代表未支付，1代表已支付，2代表挂账
	 */
	public boolean updateState(String state, String saleOrderId)
			throws Exception;

	/**
	 * @Description 修改库存
	 */
	public boolean updateInventory(String rowObj) throws Exception;

}
