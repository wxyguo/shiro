package scau.gsmg.misp.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import scau.gsmg.misp.dao.BaseDAO;
import scau.gsmg.misp.dao.IGoodsTypeDAO;
import scau.gsmg.misp.vo.GoodsType;

/**
 * @Title: GoodsTypeDAO.java
 * @Group: GaoShenMenGui
 * @Author: WXY
 * @Date: 2015-4-4
 */
@Repository
public class GoodsTypeDAO extends BaseDAO implements IGoodsTypeDAO {

	/**
	 * 从第几页获取，每页显示几行获取商品类型信息
	 */
	@Override
	public List getTypeList(String page, String rows) throws Exception {
		// TODO Auto-generated method stub
		Session session = getSession();
		String hql = "from GoodsType g";
		List dataList = new ArrayList();

		try {
			// 当为缺省值的时候进行赋值
			int currentpage = Integer
					.parseInt((page == null || page == "0") ? "1" : page);// 第几页
			int pagesize = Integer
					.parseInt((rows == null || rows == "0") ? "10" : rows);// 每页多少行
			Query query = session.createQuery(hql);
			List<GoodsType> type = query
					.setFirstResult((currentpage - 1) * pagesize)
					.setMaxResults(pagesize).list();

			for (GoodsType g : type) {
				String name = "无";
				if (g.getPid() != 0) {
					name = pName(g.getPid());
				}
				HashMap<String, Object> jsonMap = new HashMap<String, Object>();
				jsonMap.put("id", g.getId());
				jsonMap.put("pid", g.getPid());
				jsonMap.put("pname", name);
				jsonMap.put("name", g.getName());
				dataList.add(jsonMap);
			}
			return dataList;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("捕获异常：qqqqq" + e);
		} finally {
			session.close();
		}
		return null;
	}

	private String pName(int pid) {
		Session session = getSession();
		String name = "";
		try {
			String hql = "from GoodsType g where id=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, pid);
			List<GoodsType> list = query.list();
			for (GoodsType g : list) {
				name = g.getName();
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("捕获异常：" + e);
		} finally {
			session.close();
		}
		return name;
	}

	/**
	 * 统计商品类型一共有多少数据
	 */
	@Override
	public int getTypeCount() throws Exception {
		// TODO Auto-generated method stub
		Session session = getSession();
		String hql = "from GoodsType g";
		try {
			Query query = session.createQuery(hql);
			List type = query.list();
			int size = type.size();
			return size;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("捕获异常：" + e);
		} finally {
			session.close();
		}
		return 0;
	}

	/**
	 * 增加商品类型
	 */
	@Override
	public boolean addGoodsType(int pid, String name) {
		// TODO Auto-generated method stub
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		try {
			GoodsType type = new GoodsType();
			// type.setId(id);
			type.setPid(pid);
			type.setName(name);
			session.save(type);
			if (pid > 0) {
				String hql = "update GoodsType set state='1' where id=?";
				Query query = session.createQuery(hql);
				query.setParameter(0, pid);
				query.executeUpdate();
			}
			tx.commit();
			return true;
		} catch (Exception e) {
			System.out.println("addGoodsType" + e);
			return false;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	/**
	 * 更新商品类型
	 */
	@Override
	public boolean updateGoodsType(int id, String name) {
		// TODO Auto-generated method stub
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		try {
			String hql = "update GoodsType set name=? where id=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, name);
			query.setParameter(1, id);
			query.executeUpdate();
			tx.commit();
			return true;
		} catch (Exception e) {
			System.out.println("addGoodsType" + e);
			return false;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	/**
	 * 删除商品类型
	 */
	@Override
	public boolean deleteGoodsType(int id) {
		// TODO Auto-generated method stub
		boolean flag = false;
		Session session = getSession();
		try {
			parentId(id);
			String hql = "delete GoodsType g where g.id=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			query.executeUpdate();
			isChilden(id);
			flag = true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		} finally {
			session.close();
		}
		return flag;
	}

	/**
	 * @Description:根据id号获取pid
	 */
	private boolean parentId(int id) {
		Session session = getSession();
		try {
			String hql = "from GoodsType where id=?";
			Query query = session.createQuery(hql);
			query.setParameter(0, id);
			List<GoodsType> type = query.list();
			for (GoodsType g : type) {
				if (g.getPid() != null) {
					isParent(g.getPid());
				}
			}
			return true;
		} catch (Exception e) {
			System.out.println("id Parent" + e);
			return false;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	/**
	 * 判断父节点是否还存在子节点，如果不存在，将state设为0
	 */
	private boolean isParent(int pid) {
		// TODO Auto-generated method stub
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		try {
			if (pid != 0) {
				String hql = "from GoodsType g where g.pid=?";
				Query query = session.createQuery(hql);
				query.setParameter(0, pid);
				List type = query.list();
				if (type.size() == 1) {
					String hql1 = "update GoodsType set image=? where id=?";
					Query query1 = session.createQuery(hql1);
					query1.setParameter(0, "0");
					query1.setParameter(1, pid);
					query1.executeUpdate();
					tx.commit();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("isParent" + e);
		} finally {
			session.close();
		}
		return true;
	}

	/**
	 * @Description:根据id判断是否有子节点
	 */
	private boolean isChilden(int id) {
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		try {
			String hql1 = "update GoodsType set pid=? where pid=?";
			Query query1 = session.createQuery(hql1);
			query1.setParameter(0, 0);
			query1.setParameter(1, id);
			query1.executeUpdate();
			tx.commit();

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("isParent" + e);
		} finally {
			session.close();
		}
		return true;
	}
}
