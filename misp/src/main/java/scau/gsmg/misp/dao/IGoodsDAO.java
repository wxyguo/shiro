package scau.gsmg.misp.dao;

import java.util.List;

import scau.gsmg.misp.vo.Goods;
import scau.gsmg.misp.vo.GoodsType;

/**
 * @Title: IGoodsDAO.java
 * @Group: GaoShenMenGui
 * @Author: WXY
 * @Date: 2015-4-4
 */
public interface IGoodsDAO {

	/**
	 * @Description:从第几页获取，每页显示几行获取商品信息
	 */
	public List getGoodsList(String page, String rows, String sort, String order)
			throws Exception;

	/**
	 * @Description: 统计一共有多少数据
	 */
	public int getGoodsCount() throws Exception;

	/**
	 * @Description:商品类型树节点Combotree
	 */
	public List<GoodsType> combotree();

	/**
	 * @Description:增加商品
	 */
	public boolean addGoods(Goods goods);

	/**
	 * @Description:更新商品by id
	 */
	public boolean updateGoods(Goods goods);

	/**
	 * @Description:删除商品
	 */
	public boolean removeGoods(int id);

	/**
	 * @Description:更新商品by name
	 */
	public boolean updateGoods(int id, String barcode, String type_id,
			String name, double purchase_price, double price,
			String description, String unit);

	/**
	 * @Description:搜索商品
	 */
	public List searchGoods(String page, String rows, String barcode,
			String name, String typeName, String sort, String order);

	/**
	 * @Description:搜索出的商品的总数
	 */
	public int searchCount(String barcode, String name, String typeName);
}
