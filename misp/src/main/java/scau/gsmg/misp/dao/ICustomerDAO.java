package scau.gsmg.misp.dao;

import java.util.Date;
import java.util.List;

/**
 * @Title ICustomerDAO.java
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */
public interface ICustomerDAO {
	/**
	 * @Description 从第几页获取，每页显示几行获取会员信息
	 */
	public List getCustomerList(String page, String rows, String sort,
			String order) throws Exception;

	/**
	 * @Description 添加新会员
	 */
	public boolean addCustomer(String name, String gender, String cellphone,
			String address, double discount, int integral) throws Exception;

	/**
	 * @Description 修改会员信息
	 */
	public boolean editCustomer(int id, String name, String gender,
			String cellphone, String address, double discount, int integral)
			throws Exception;

	/**
	 * @Description 删除会员
	 */
	public boolean removeCustomer(int id) throws Exception;

	/**
	 * @Description 搜索会员
	 */
	public List searchCustomer(String page, String rows, int id, String name,
			String cellphone, String sort, String order) throws Exception;

	/**
	 * @Description 通过注册日期区间搜索会员
	 */
	public List searchByDate(Date firstDate, Date lastDate, String page,
			String rows, String sort, String order) throws Exception;

	/**
	 * @Description 通过统计记录数
	 */
	public int CountRecords(String hql, Object[] parameters) throws Exception;
}
