package scau.gsmg.misp.dao;

import java.util.List;

/**
 * @Title: IGoodsTypeDAO.java
 * @Group: GaoShenMenGui
 * @Author: WXY
 * @Date: 2015-4-4
 */
public interface IGoodsTypeDAO {

	/**
	 * @Description:从第几页获取，每页显示几行获取商品类型信息
	 */
	public List getTypeList(String page, String rows) throws Exception;

	/**
	 * @Description:统计商品类型一共有多少数据
	 */
	public int getTypeCount() throws Exception;

	/**
	 * @Description:增加商品类型
	 */
	public boolean addGoodsType(int pid, String name);

	/**
	 * @Description:更新商品类型
	 */
	public boolean updateGoodsType(int id, String name);

	/**
	 * @Description:删除商品类型
	 */
	public boolean deleteGoodsType(int id);
}
