package scau.gsmg.misp.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import scau.gsmg.misp.service.IGoodsService;
import scau.gsmg.misp.vo.Goods;
import scau.gsmg.misp.vo.GoodsType;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @Title: GoodsAction.java
 * @Group: GaoShenMenGui
 * @Author: WXY
 * @Date: 2015-4-4
 */

@ParentPackage("json-default")
@Namespace("/")
@Scope("prototype")
@Results({
		@Result(name = "success", type = "json", params = { "root", "jsonObj" }),
		@Result(name = "jsonTree", type = "json", params = { "root", "jsonTree" }),
		@Result(name = "goodsEdit", location = "/editGoods.jsp") })
@Controller
public class GoodsAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private JSONObject jsonObj;
	@Resource
	protected IGoodsService goodsService;
	private String page;
	private String rows;
	private String sort;
	private String order;
	private JSON jsonTree;
	private int id;
	private int tid;
	private String barcode;

	private String typeName;
	private String name;
	private double purchasePrice;
	private double price;
	private String description;
	private String unit;

	@Action(value = "goodsEdit")
	public String goodEdit() {
		return "goodsEdit";
	}

	/**
	 * @Description:查询出所有商品信息
	 */
	@Action(value = "getAllGoods")
	public String getAllGoods() throws Exception {
		List list = goodsService.getGoodsList(page, rows, sort, order);
		int count = goodsService.getGoodsCount();
		this.toBeJson(list, count);
		return "success";
	}

	/**
	 * @Description: 转化为Json格式
	 */
	public void toBeJson(List list, int total) throws Exception {

		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		JSONObject data = new JSONObject();
		data.put("total", total);
		data.put("rows", list);
		jsonObj = data;
		response.setCharacterEncoding("utf-8");

	}

	/**
	 * @Description:类型树节点Combotree
	 */
	@Action(value = "combotree")
	public String combotree() {
		List<GoodsType> goodsType = goodsService.combotree();
		this.jsonTree = createCombotree(goodsType, 0);
		return "jsonTree";
	}

	private JSON createCombotree(List<GoodsType> goodsType, int pid) {
		List list = new ArrayList();
		Map<String, Object> jsonMap = null; // map对象模拟json键值对
		for (GoodsType type : goodsType) {
			if (type.getPid() != null && (pid == type.getPid())) {
				jsonMap = new HashMap<String, Object>();
				jsonMap.put("id", type.getId());
				jsonMap.put("text", type.getName());

				if ("1".equals(type.getState())) {// =1表示有子节点
					jsonMap.put("state", "closed");
					jsonMap.put("children",
							createCombotree(goodsType, type.getId()));// 载入子节点
				}
				list.add(jsonMap);
			}
		}

		return JSONArray.fromObject(list);// map转为json
	}

	/**
	 * @Description:增加商品
	 */
	@Action(value = "addGoods")
	public String addGoods() {

		Goods goods = new Goods();
		GoodsType goodsType = new GoodsType();
		goodsType.setId(tid);
		goods.setBarcode(barcode);
		goods.setGoodsType(goodsType);
		goods.setName(name);
		goods.setPurchasePrice(purchasePrice);
		goods.setPrice(price);
		goods.setUnit(unit);
		goods.setDescription(description);
		boolean param = goodsService.addGoods(goods);
		JSONObject data = new JSONObject();
		if (param) {
			data.put("success", param);
			data.put("message", "商品: " + goods.getName() + " 增加成功！");
		} else {
			data.put("success", param);
			data.put("message", "商品: " + goods.getName() + " 增加失败！");
		}
		this.jsonObj = data;
		return SUCCESS;
	}

	/**
	 * @Description:更新商品 by id
	 */
	@Action(value = "renewGoods")
	public String renewGoods() {

		Goods goods = new Goods();
		goods.setId(id);
		GoodsType goodsType = new GoodsType();
		goodsType.setId(tid);
		goods.setBarcode(barcode);
		goods.setGoodsType(goodsType);
		goods.setName(name);
		goods.setPurchasePrice(purchasePrice);
		goods.setPrice(price);
		goods.setUnit(unit);
		goods.setDescription(description);
		boolean param = goodsService.updateGoods(goods);
		JSONObject data = new JSONObject();
		if (param) {
			data.put("success", param);
			data.put("message", "商品: " + goods.getName() + " 更新成功！");
		} else {
			data.put("success", param);
			data.put("message", "商品: " + goods.getName() + " 更新失败！");
		}

		this.jsonObj = data;
		return SUCCESS;
	}

	/**
	 * @Description:删除商品
	 */
	@Action(value = "removeGoods")
	public String removeGoods() {
		JSONObject data = new JSONObject();
		if (goodsService.removeGoods(id)) {
			data.put("success", true);
			data.put("message", "成功删除商品！");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "商品删除失败！");
			jsonObj = data;
			return "success";
		}
	}

	/**
	 * @Description:搜索商品
	 */
	@Action(value = "searchGoods")
	public String searchGoods() throws Exception {
		if (name != null) {
			// 解码
			name = java.net.URLDecoder.decode(name, "utf-8");
		}
		if (typeName != null) {
			// 解码
			typeName = java.net.URLDecoder.decode(typeName, "utf-8");
		}
		System.out.println(typeName + "typeName");
		List list = goodsService.searchGoods(page, rows, barcode, name,
				typeName, sort, order);
		int count = 1;//
		count = goodsService.searchCount(barcode, name, typeName);
		this.toBeJson(list, count);
		return "success";
	}

	public JSONObject getJsonObj() {
		return jsonObj;
	}

	public void setJsonObj(JSONObject jsonObj) {
		this.jsonObj = jsonObj;
	}

	public IGoodsService getGoodsService() {
		return goodsService;
	}

	public void setGoodsService(IGoodsService goodsService) {
		this.goodsService = goodsService;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public JSON getJsonTree() {
		return jsonTree;
	}

	public void setJsonTree(JSON jsonTree) {
		this.jsonTree = jsonTree;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public String getSort() {
		return sort;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

}
