package scau.gsmg.misp.action;

import java.util.List;
import java.sql.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.ExceptionMappings;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.json.annotations.JSON;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.opensymphony.xwork2.ActionSupport;

import scau.gsmg.misp.dao.impl.CheckoutDAO;
import scau.gsmg.misp.service.ICustomerService;

/**
 * @Title 管理员对顾客的所有操作
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */

@Controller
@Scope("prototype")
@Namespace(value = "/")
@ParentPackage("json-default")
@Results({
		@Result(name = "success", type = "json", params = { "root", "jsonObj" }),
		@Result(name = "error", location = "/error.jsp") })
// @ExceptionMappings( { @ExceptionMapping(exception =
// "java.lang.RuntimeException", result = "error") })
public class CustomerAction extends ActionSupport {
	@Resource
	protected ICustomerService customerService;
	private JSONObject jsonObj;
	private String rows;// 每页显示的记录数
	private String page;// 当前第几页
	private String name;
	private String gender;
	private String cellphone;
	private String address;
	private double discount;
	private int integral;
	private int id;
	private String sort;
	private String order;
	private Date firstDate;
	private Date lastDate;
	private static final Logger logger = Logger.getLogger(CustomerAction.class);

	/**
	 * @Description 查询出所有顾客信息
	 */
	@Action(value = "getAllCustomer")
	public String getAllCustomer() throws Exception {
		List list = customerService.getCustomerList(page, rows, sort, order);
		int count = customerService.CountTotalRecords();// 总记录数
		this.toBeJson(list, count);
		return "success";
	}

	/**
	 * @Description:转化为Json格式
	 */
	public void toBeJson(List list, int total) throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();

		JSONObject data = new JSONObject();
		data.put("total", total);
		data.put("rows", list);
		jsonObj = data;
		response.setCharacterEncoding("utf-8");
	}

	/**
	 * @Description 添加新会员
	 */
	@Action(value = "addCustomer")
	public String addCustomer() throws Exception {
		JSONObject data = new JSONObject();
		if (customerService.addCustomer(name, gender, cellphone, address,
				discount, integral)) {
			data.put("success", true);
			data.put("message", "新会员:" + name + " 添加成功！");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "会员添加失败！");
			jsonObj = data;
			return "success";
		}
	}

	/**
	 * @Description 修改会员信息
	 */
	@Action(value = "editCustomer")
	public String editCustomer() throws Exception {
		JSONObject data = new JSONObject();
		if (customerService.editCustomer(id, name, gender, cellphone, address,
				discount, integral)) {
			data.put("success", true);
			data.put("message", "成功修改会员信息！");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "修改信息失败！");
			jsonObj = data;
			return "success";
		}
	}

	/**
	 * @Description 删除会员
	 */
	@Action(value = "removeCustomer")
	public String removeCustomer() throws Exception {
		JSONObject data = new JSONObject();
		if (customerService.removeCustomer(id)) {
			data.put("success", true);
			data.put("message", "成功删除一位会员！");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "删除失败！");
			jsonObj = data;
			return "success";
		}
	}

	/**
	 * @Description 搜索会员
	 */
	@Action(value = "searchCustomer")
	public String searchCustomer() throws Exception {
		if (name != null) {
			// 解码
			name = java.net.URLDecoder.decode(name, "utf-8");
		}
		List list = customerService.searchCustomer(page, rows, id, name,
				cellphone, sort, order);
		int count = 0;
		if (list != null) {
			count = customerService.CountTotalSearch(id, name, cellphone);
		}
		this.toBeJson(list, count);
		// 把这两个值清空，不然搜索的时候会现严重出问题
		/*
		 * id=0; name=null;
		 */
		return "success";
	}

	/**
	 * @Description 通过日期区间搜索会员
	 */
	@Action(value = "searchByDate")
	public String searchByDate() throws Exception {
		List list = customerService.searchByDate(firstDate, lastDate, page,
				rows, sort, order);
		int count = 0;
		if (list.size() > 0) {
			count = customerService.CountRecordsByDateArea(firstDate, lastDate);// 总记录数
		}
		this.toBeJson(list, count);
		// 把这两个值清空，不然搜索的时候会现严重出问题
		/*
		 * id=0; name=null;
		 */
		return "success";
	}

	public ICustomerService getCustomerService() {
		return customerService;
	}

	@Resource
	public void setCustomerService(ICustomerService customerService) {
		this.customerService = customerService;
	}

	public JSONObject getJsonObj() {
		return jsonObj;
	}

	public void setJsonObj(JSONObject jsonObj) {
		this.jsonObj = jsonObj;
	}

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public int getIntegral() {
		return integral;
	}

	public void setIntegral(int integral) {
		this.integral = integral;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Date getFirstDate() {
		return firstDate;
	}

	public void setFirstDate(Date firstDate) {
		this.firstDate = firstDate;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

}
