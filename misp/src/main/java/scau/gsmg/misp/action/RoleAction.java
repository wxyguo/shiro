package scau.gsmg.misp.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.opensymphony.xwork2.ActionSupport;

import scau.gsmg.misp.service.IRoleService;
import scau.gsmg.misp.vo.Role;

/**
 * @Title 管理员对角色的所有管理操作
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */

@Controller
@Scope("prototype")
@Namespace(value = "/")
@ParentPackage("json-default")
@Results({ @Result(name = "success", type = "json", params = { "root",
		"jsonObj" }) })
public class RoleAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5226296293225371289L;
	protected IRoleService roleService;
	private JSONObject jsonObj;
	private String rows;// 每页显示的记录数
	private String page;// 当前第几页
	private String sort;
	private String order;
	private Role role;
	private int id;

	/**
	 * @Description:查询出所有角色信息
	 */
	@Action(value = "getAllRole")
	public String getAllRole() throws Exception {
		List list = roleService.getAllRole(page, rows, sort, order);
		int count = roleService.getRoleCount();// 得到总记录数
		this.toBeJson(list, count);
		return "success";
	}

	/**
	 * @Description:toBeJson方法：转化为Json格式
	 */
	public void toBeJson(List list, int total) throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();

		JSONObject data = new JSONObject();
		data.put("total", total);
		data.put("rows", list);
		jsonObj = data;
		response.setCharacterEncoding("utf-8");
	}

	/**
	 * @Description 添加新角色
	 */
	@Action(value = "addRole")
	public String addRole() throws Exception {
		JSONObject data = new JSONObject();
		if (roleService.addRole(role)) {
			data.put("success", true);
			data.put("message", "成功添加角色：" + role.getName() + "!");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "添加失败！");
			jsonObj = data;
			return "success";
		}
	}

	/**
	 * @Description 修改角色
	 */
	@Action(value = "editRole")
	public String editRole() throws Exception {
		JSONObject data = new JSONObject();
		if (roleService.editRole(role, id)) {
			data.put("success", true);
			data.put("message", "修改成功!");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "修改失败！");
			jsonObj = data;
			return "success";
		}
	}

	/**
	 * @Description 删除角色
	 */
	@Action(value = "removeRole")
	public String removeRole() throws Exception {
		JSONObject data = new JSONObject();
		if (roleService.removeRole(id)) {
			data.put("success", true);
			data.put("message", "删除成功!");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "删除失败！");
			jsonObj = data;
			return "success";
		}
	}

	public IRoleService getRoleService() {
		return roleService;
	}

	@Resource
	public void setRoleService(IRoleService roleService) {
		this.roleService = roleService;
	}

	public JSONObject getJsonObj() {
		return jsonObj;
	}

	public void setJsonObj(JSONObject jsonObj) {
		this.jsonObj = jsonObj;
	}

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
