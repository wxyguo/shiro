package scau.gsmg.misp.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSON;
import net.sf.json.JSONArray;

import org.apache.shiro.SecurityUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import scau.gsmg.misp.service.IMenuService;
import scau.gsmg.misp.utils.ShiroUtil;
import scau.gsmg.misp.vo.Permission;

import com.alibaba.fastjson.JSONObject;
import com.opensymphony.xwork2.ActionSupport;

/**
 * @Title: MenuAction.java
 * @Group: GaoShenMenGui
 * @Author: WXY
 * @Date: 2015-5-9
 */
@ParentPackage("json-default")
@Namespace("/")
@Scope("prototype")
@Results({
		@Result(name = "success", type = "json", params = { "root", "root" }),
		@Result(name = "goodsEdit", location = "/editGoods.jsp") ,
		@Result(name = "jsonArray", type = "json", params = { "root",
		"jsonArray" }) 
		})
@Controller
public class MenuAction extends ActionSupport {

	private String success;
	private JSON root;
	@Resource
	private IMenuService menuService;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	public JSONObject getJsonObject() {
		return jsonObject;
	}
	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}
	public JSONArray getJsonArray() {
		return jsonArray;
	}
	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public IMenuService getMenuService() {
		return menuService;
	}
	public void setMenuService(IMenuService menuService) {
		this.menuService = menuService;
	}
	public JSON getRoot() {
		return root;
	}
	public void setRoot(JSON root) {
		this.root = root;
	}
	/*@Action(value = "treedata")
	public String searchMenu(){
//		this.root=menuService.searchMenu();
		List list=menuService.searchMenu(loginID);
		//JSONArray data = JSONArray.fromObject(list);
		//this.jsonArray = data;
	//	return "jsonArray";
		return "success";
		
	}*/
	@Action(value = "tree")
	public String showMenu(){
//		this.root=menuService.searchMenu();
		List list=null;
		JSON data =null;
		System.out.println("WXY"+SecurityUtils.getSubject().isAuthenticated());
		if(SecurityUtils.getSubject().isAuthenticated()){
			System.out.println("First"+SecurityUtils.getSubject().isAuthenticated()+" uuu"+SecurityUtils.getSubject().getSession());
			list=menuService.showMenu(ShiroUtil.getCurrentUser().getId());
		}
		
		//List list=menuService.showMenu();
		if(list!=null){
			System.out.println("List list=menuService.showMenu();"+list.size());
			data = JSONArray.fromObject(list);
			System.out.println(data.toString()+"&&7");
		}else{
			String str="[{ \"id\":1,\"text\":\"没有权限\"}]";
			//list.add(str);
			data = JSONArray.fromObject(str);
		}
       
		this.root = data;
		System.out.println("pppp");
		//return "jsonArray";
		return "success";
		
	}
}
