package scau.gsmg.misp.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.struts2.ServletActionContext;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSON;
import net.sf.json.JSONArray;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;

import scau.gsmg.misp.service.IUserService;
import scau.gsmg.misp.vo.Role;
import scau.gsmg.misp.vo.User;
import scau.gsmg.misp.vo.UserRole;

@Controller
@Scope("prototype")
@Namespace(value = "/")
@ParentPackage("json-default")
@Results({
		@Result(name = "success", type = "json", params = { "root", "jsonObj" }),
		@Result(name = "jsonArray", type = "json", params = { "root",
				"jsonArray" }) })
public class UserAction {
	protected IUserService userService;
	private JSONObject jsonObj;
	private String rows;// 每页显示的记录数
	private String page;// 当前第几页
	private String sort;
	private String order;
	private JSONArray jsonArray;
	private User user;
	private int id;
	private int roleId;
	private String roleName;
	private String name;

	/**
	 * @Description:查询出所有顾客信息
	 */
	@Action(value = "getAllUser")
	public String getAllUser() throws Exception {
		// System.out.println("sort" + sort + "order" + order);
		List list = userService.getAllUser(page, rows, sort, order);
		int count = userService.getUserCount();// 得到总记录数
		this.toBeJson(list, count);
		return "success";
	}

	/**
	 * @Description toBeJson方法：转化为Json格式
	 */
	public void toBeJson(List list, int total) throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();

		JSONObject data = new JSONObject();
		data.put("total", total);
		data.put("rows", list);
		jsonObj = data;
		// System.out.println(jsonObj.toString());
		response.setCharacterEncoding("utf-8");
	}

	/**
	 * @throws Exception
	 * @Description:获取role角色
	 */
	@Action(value = "userCombobox")
	public String userCombobox() throws Exception {
		// System.out.println("JSONArray");
		List role = userService.userCombobox();
		// System.out.println(role.size());
		JSONArray data = JSONArray.fromObject(role);
		this.jsonArray = data;
		// System.out.println(jsonArray);
		return "jsonArray";
	}

	/**
	 * @Description:添加用户
	 */
	@Action(value = "addUser")
	public String addUser() throws Exception {

		boolean param = userService.addUser(user, roleId);
		JSONObject data = new JSONObject();
		if (param) {
			data.put("success", param);
			data.put("message", "用户: " + user.getName() + " 增加成功！");
		} else {
			data.put("success", param);
			data.put("message", "用户: " + user.getName() + " 增加失败！");
		}
		this.jsonObj = data;
		return "success";
	}

	/**
	 * @Description:修改用户
	 */
	@Action(value = "editUser")
	public String editUser() throws Exception {

		// System.out.println("EDITUSER"+roleName);
		boolean param = userService.editUser(id, roleId, user);
		JSONObject data = new JSONObject();
		if (param) {
			data.put("success", param);
			data.put("message", "用户: " + user.getName() + " 更新成功！");
		} else {
			data.put("success", param);
			data.put("message", "用户: " + user.getName() + " 更新失败！");
		}
		this.jsonObj = data;
		return "success";
	}

	/**
	 * @Description:删除用户
	 */
	@Action(value = "removeUser")
	public String removeUser() throws Exception {
		JSONObject data = new JSONObject();
		boolean param = userService.removeUser(id);
		// System.out.println(param+"*****");
		if (param) {
			data.put("success", true);
			data.put("message", "成功删除用户！");
			jsonObj = data;
			return "success";
		} else {
			data.put("success", false);
			data.put("message", "用户删除失败！");
			jsonObj = data;
			return "success";
		}
	}

	/**
	 * @Description:搜索用户
	 */
	@Action(value = "searchUser")
	public String searchUser() throws Exception {
		// System.out.println("****"+name);
		if (name != null) {
			// 解码
			name = java.net.URLDecoder.decode(name, "utf-8");
			// System.out.println(name);
		}
		// System.out.println("ll"+name+ id+name+value);
		List list = userService.searchUser(page, rows, id, name, sort, order);
		// System.out.println("datalistaaaa"+list);
		int count = 1;//
		count = userService.searchCount(id, name);
		// System.out.println("count"+count);
		this.toBeJson(list, count);
		return "success";
	}

	/**
	 * @Description:login
	 */
	/*@Action(value = "login")
	public String login() throws Exception {
		// System.out.println("****"+name);
		if (name != null) {
			// 解码
			name = java.net.URLDecoder.decode(name, "utf-8");
			// System.out.println(name);
		}
		// System.out.println("ll"+name+ id+name+value);
		List list = userService.searchUser(page, rows, id, name, sort, order);
		// System.out.println("datalistaaaa"+list);
		int count = 1;//
		count = userService.searchCount(id, name);
		// System.out.println("count"+count);
		this.toBeJson(list, count);
		return "success";
	}*/
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public JSONObject getJsonObj() {
		return jsonObj;
	}

	public void setJsonObj(JSONObject jsonObj) {
		this.jsonObj = jsonObj;
	}

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public IUserService getUserService() {
		return userService;
	}

	@Resource
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
