package scau.gsmg.misp.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import scau.gsmg.misp.service.ICustomerService;
import scau.gsmg.misp.service.IGoodsTypeService;
import net.sf.json.JSONObject;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("json-default")
@Namespace("/")
@Scope("prototype")
@Results({ @Result(name = "success", type = "json", params = { "root",
		"jsonObj" }) })
@Controller
public class GoodsTypeAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private String pname;
	private int id;
	private int pid = 0;
	private String name;
	private String page;
	private String rows;
	private JSONObject jsonObj;
	@Resource
	protected IGoodsTypeService goodsTypeService;

	/**
	 * @Description:查询出所有商品信息
	 */
	@Action(value = "getAllType")
	public String allType() throws Exception {

		List list = goodsTypeService.getTypeList(page, rows);
		int count = goodsTypeService.getTypeCount();

		this.toBeJson(list, count);
		return "success";
	}

	/**
	 * @Description:转化为Json格式
	 */
	public void toBeJson(List list, int total) throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();

		JSONObject data = new JSONObject();
		data.put("total", total);
		data.put("rows", list);
		jsonObj = data;
		response.setCharacterEncoding("utf-8");

	}

	/**
	 * @Description:商品类型增加
	 */
	@Action(value = "addGoodsType")
	public String addGoodsType() {
		boolean param = goodsTypeService.addGoodsType(pid, name);
		JSONObject data = new JSONObject();
		if (param) {
			data.put("success", param);
			data.put("message", "产品类型: " + name + " 添加成功！");
		} else {
			data.put("success", param);
			data.put("message", "产品类型: " + name + " 添加失败！");
		}
		this.jsonObj = data;
		return SUCCESS;

	}

	/**
	 * @Description:商品类型更新
	 */
	@Action(value = "updateGoodsType")
	public String updateGoodsType() {
		boolean param = goodsTypeService.updateGoodsType(id, name);
		JSONObject data = new JSONObject();
		if (param) {
			data.put("success", param);
			data.put("message", "产品类型: " + name + " 更新成功！");
		} else {
			data.put("success", param);
			data.put("message", "产品类型: " + name + " 更新失败！");
		}
		this.jsonObj = data;
		return SUCCESS;
	}

	/**
	 * @Description:商品类型删除
	 */
	@Action(value = "deleteGoodsType")
	public String deleteGoodsType() {
		boolean param = goodsTypeService.deleteGoodsType(id);
		JSONObject data = new JSONObject();
		if (param) {
			data.put("success", param);
			data.put("message", "产品类型id为: " + id + " 删除成功！");
		} else {
			data.put("success", param);
			data.put("message", "产品类型id为: " + id + " 删除失败！");
		}
		this.jsonObj = data;
		return SUCCESS;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public JSONObject getJsonObj() {
		return jsonObj;
	}

	public void setJsonObj(JSONObject jsonObj) {
		this.jsonObj = jsonObj;
	}

	public IGoodsTypeService getGoodsTypeService() {
		return goodsTypeService;
	}

	public void setGoodsTypeService(IGoodsTypeService goodsTypeService) {
		this.goodsTypeService = goodsTypeService;
	}

	private ICustomerService customerService;

	public ICustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(ICustomerService customerService) {
		this.customerService = customerService;
	}

}
