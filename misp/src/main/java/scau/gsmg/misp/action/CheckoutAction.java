package scau.gsmg.misp.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.opensymphony.xwork2.ActionSupport;

import scau.gsmg.misp.service.ICheckoutService;
import scau.gsmg.misp.vo.Goods;
import scau.gsmg.misp.vo.SaleOrder;

import org.apache.log4j.Logger;

/**
 * @Title 收银员的所有操作
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月9日
 */
@Controller
@Scope("prototype")
@Namespace(value = "/")
@ParentPackage("json-default")
@Results({
		@Result(name = "success", type = "json", params = { "root", "jsonArray" }),
		@Result(name = "tip", type = "json", params = { "root", "jsonObj" }),
		@Result(name = "error", location = "/error.jsp") })
public class CheckoutAction extends ActionSupport {

	private static final long serialVersionUID = 3949178453004890756L;
	protected ICheckoutService checkoutService;
	private JSONObject jsonObj;
	private Goods goods;
	private JSONArray jsonArray;
	private String barcode;
	private String rowObj;
	private int customerId;
	private int cashierId;
	private int suspendId;
	private String saleId;
	private double totalPrice;
	private double realPay;
	private double change;
	private double receivables;
	private int amount;
	private String paymentId;
	private String paymentMethod;

	private static final Logger logger = Logger.getLogger(CheckoutAction.class);

	/**
	 * @Description 收银员通过商品条码搜索商品
	 */
	@Action(value = "searchGoodsById")
	public String searchGoodsById() throws Exception {
		List list = checkoutService.searchGoodsById(barcode);
		jsonArray = net.sf.json.JSONArray.fromObject(list);
		if (jsonArray.toString().equals("[null]")) {
			jsonArray.add("商品不存在！");
			return "tip";
		}
		return "success";
	}

	/**
	 * @Description:转化为Json格式
	 */
	public void toBeJson(List list, int total) throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		JSONObject data = new JSONObject();
		data.put("total", total);
		data.put("rows", list);
		jsonObj = data;
		response.setCharacterEncoding("utf-8");
	}

	/**
	 * @Description 得到一个新的销售单号
	 */
	@Action(value = "gainOrder")
	public String gainOrder() throws Exception {
		String orderId = checkoutService.gainOrder("so");// 得到销售号
		JSONObject data = new JSONObject();
		data.put("success", true);
		data.put("message", orderId);
		jsonObj = data;
		return "tip";
	}

	/**
	 * @Description 得到一个新的支付单号
	 */
	@Action(value = "gainPaymentId")
	public String gainPaymentId() throws Exception {
		String paymentId = checkoutService.gainOrder("sp");// 得到支付单号
		JSONObject data = new JSONObject();
		data.put("success", true);
		data.put("message", paymentId);
		jsonObj = data;
		return "tip";
	}

	/**
	 * @Description 挂账
	 */
	@Action(value = "suspendBill")
	public String suspendBill() throws Exception {
		String msg = "";
		if (rowObj != null) {
			msg = checkoutService.suspendBill(rowObj, customerId, totalPrice,
					saleId, amount, receivables);
		}
		JSONObject data = new JSONObject();
		data.put("success", true);
		data.put("message", msg);
		jsonObj = data;
		return "tip";
	}

	/**
	 * @Description 提取单据
	 */
	@Action(value = "gainSuspend")
	public String gainSuspend() throws Exception {
		List<SaleOrder> list = checkoutService.gainSuspend();
		int count = 0;
		if (list.size() > 0) {
			count = list.size();// 总记录数
		}
		this.toBeJson(list, count);
		return "tip";
	}

	/**
	 * @Description 获取单据中的所有商品信息
	 */
	@Action(value = "gainSuspendGoods")
	public String gainSuspendGoods() throws Exception {
		List list = checkoutService.gainSuspendGoods(suspendId);
		int count = list.size();
		this.toBeJson(list, count);
		return "tip";
	}

	/**
	 * @Description 提取账单后，删除该记录
	 */
	@Action(value = "deleteRecord")
	public String deleteRecord() throws Exception {
		JSONObject data = new JSONObject();
		if (checkoutService.deleteRecord(suspendId)) {
			data.put("success", true);
			data.put("message", "成功提取单据！");
			jsonObj = data;
			return "tip";
		} else {
			data.put("success", false);
			data.put("message", "失败！");
			jsonObj = data;
			return "tip";
		}
	}

	/**
	 * @Description 销售开单
	 */
	@Action(value = "makeSaleOrder")
	public String makeSaleOrder() throws Exception {
		JSONObject data = new JSONObject();
		if (checkoutService.makeSaleOrder(rowObj, customerId, totalPrice,
				saleId, amount, receivables)) {
			data.put("success", true);
			jsonObj = data;
			return "tip";
		} else {
			data.put("success", false);
			jsonObj = data;
			return "tip";
		}
	}

	/**
	 * @Description 结算
	 */
	@Action(value = "checkout")
	public String checkout() throws Exception {
		JSONObject data = new JSONObject();
		if (checkoutService.checkout(cashierId, totalPrice, realPay, change,
				saleId, paymentMethod, paymentId)) {
			// 修改库存
			checkoutService.updateInventory(rowObj);
			data.put("success", true);
			data.put("message", "结算成功！");
			jsonObj = data;
			return "tip";
		} else {
			data.put("success", false);
			data.put("message", "失败！");
			jsonObj = data;
			return "tip";
		}
	}

	public ICheckoutService getCheckoutService() {
		return checkoutService;
	}

	@Resource
	public void setCheckoutService(ICheckoutService checkoutService) {
		this.checkoutService = checkoutService;
	}

	public JSONObject getJsonObj() {
		return jsonObj;
	}

	public void setJsonObj(JSONObject jsonObj) {
		this.jsonObj = jsonObj;
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public net.sf.json.JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(net.sf.json.JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getRowObj() {
		return rowObj;
	}

	public void setRowObj(String rowObj) {
		this.rowObj = rowObj;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getSuspendId() {
		return suspendId;
	}

	public void setSuspendId(int suspendId) {
		this.suspendId = suspendId;
	}

	public int getCashierId() {
		return cashierId;
	}

	public void setCashierId(int cashierId) {
		this.cashierId = cashierId;
	}

	public String getSaleId() {
		return saleId;
	}

	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getRealPay() {
		return realPay;
	}

	public void setRealPay(double realPay) {
		this.realPay = realPay;
	}

	public double getChange() {
		return change;
	}

	public void setChange(double change) {
		this.change = change;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public double getReceivables() {
		return receivables;
	}

	public void setReceivables(double receivables) {
		this.receivables = receivables;
	}

}
