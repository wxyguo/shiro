package scau.gsmg.misp.action;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.AccountLockedException;

import net.sf.json.JSONObject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import scau.gsmg.misp.utils.IncorrectCaptchaException;
import scau.gsmg.misp.utils.ShiroUtil;
import scau.gsmg.misp.vo.User;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("json-default")
@Scope("prototype")
@Namespace(value = "/")

@Results({
	@Result(name="loginSuccess",type="json",params={"root","jsonObject"}),
//	@Result(name="errorReturn",params={"errorException"}),
	@Result(name="logout", type="redirect", location="/index.jsp")})
@Controller
public class LoginAction extends ActionSupport{

	/**
	 * 
	 */
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();
	
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String rememberMe;
	private JSONObject jsonObject;
	private List<User> userList;
	private String errorException;
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	public String getErrorException() {
		return errorException;
	}

	public void setErrorException(String errorException) {
		this.errorException = errorException;
	}

	/*@Action(value="login", results={@Result(name=LOGIN, type="redirect", location="list.action"),
			@Result(name=ERROR, location="/error.jsp")})*/
	@Action(value="login")
	public String login() {
		
		System.out.println("LOGININGINGING");
		//logger.info("登陆");
		System.out.println("登陆");
        String exceptionClassName = (String) request.getAttribute("shiroLoginFailure");
        String errorException = null;
        String message = AuthenticationException.class.getSimpleName();
        System.out.println("NEWNEW"+message+"  "+exceptionClassName+"  "+IncorrectCredentialsException.class.getSimpleName());
        Subject subject = SecurityUtils.getSubject();
        System.out.println("Subject subject = SecurityUtils.getSubject();");
        if (subject.isAuthenticated()) {//已认证且用户手动打开登录页
        	System.out.println("SUccessSUccess5555");
        	JSONObject json=new JSONObject();
        	json.put("success", true);
    		jsonObject=json;
        	
        } else {
            if ("scau.gsmg.misp.utils.IncorrectCaptchaException".equals(exceptionClassName)) {
                errorException = "验证码错误！";
            } else if ("org.apache.shiro.authc.UnknownAccountException".equals(exceptionClassName)) {
                errorException = "用户名不存在！";
            }else if ("org.apache.shiro.authc.IncorrectCredentialsException".equals(exceptionClassName)) {
            	errorException = "密码错误！";
            }
            else if (AccountLockedException.class.getSimpleName().equals(exceptionClassName)) {
                errorException = "帐号已经锁定";
            }else{
                errorException = "其他错误！请稍后重试！";
            }
           // errorException=IncorrectCredentialsException.class.getSimpleName();
            System.out.println("SUccessSUccess5555");
        	JSONObject json=new JSONObject();
        	json.put("success",errorException);
    		jsonObject=json;
        }
        System.out.println(errorException);
       // request.setAttribute("error", error);
		//return "errorReturn";
        return "loginSuccess";
	}
	
//	@Action(value="list", results={@Result(name="loginSuccess",type="json",params={"root","jsonObject"})})
/*	@Action(value="list")
	public String list() { // 这个是列表方法
		System.out.println(SecurityUtils.getSubject().hasRole("admin") + "   0000000000000000000000000");
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("success", true);
		return "loginSuccess";
	}*/
	
	
	
//	@Action(value="logout", results={@Result(name="logout", type="redirect", location="/login.jsp")})
	@Action(value="logout")
	public String logout() {

		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		ShiroUtil.getSession().removeAttribute("user");
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session1 = request.getSession();
		session1.invalidate();
		System.out.println();
		System.out.println("loginout");
		return "logout";
	}
	public String fail(){
		return null;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRememberMe() {
		return rememberMe;
	}
	public void setRememberMe(String rememberMe) {
		this.rememberMe = rememberMe;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}
}
