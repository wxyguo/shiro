package scau.gsmg.misp.service;

import java.util.List;
import java.util.Date;

/**
 * @Title ICustomerService.java
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */
public interface ICustomerService {

	/**
	 * @Description 从第几页获取，每页显示几行获取会员信息
	 */
	public List getCustomerList(String page, String rows, String sort,
			String order) throws Exception;

	/**
	 * @Description 添加新会员
	 */
	public boolean addCustomer(String name, String gender, String cellphone,
			String address, double discount, int integral) throws Exception;

	/**
	 * @Description 修改会员信息
	 */
	public boolean editCustomer(int id, String name, String gender,
			String cellphone, String address, double discount, int integral)
			throws Exception;

	/**
	 * @Description 删除会员
	 */
	public boolean removeCustomer(int id) throws Exception;

	/**
	 * @Description 搜索会员
	 */
	public List searchCustomer(String page, String rows, int id, String name,
			String cellphone, String sort, String order) throws Exception;

	/**
	 * @Description 通过注册日期区间搜索会员
	 */
	public List searchByDate(Date firstDate, Date lastDate, String page,
			String rows, String sort, String order) throws Exception;

	/**
	 * @Description 统计记录数
	 */
	/* public int CountRecords(String hql,Object[] parameters) throws Exception; */
	/**
	 * @Description 统计总的记录数
	 */
	public int CountTotalRecords() throws Exception;

	/**
	 * @Description 通过日期区间统计记录数
	 */
	public int CountRecordsByDateArea(Date firstDate, Date lastDate)
			throws Exception;

	/**
	 * @Description 统计搜索返回的记录数
	 */
	public int CountTotalSearch(int id, String name, String cellphone)
			throws Exception;
}
