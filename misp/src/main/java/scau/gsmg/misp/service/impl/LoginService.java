package scau.gsmg.misp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import scau.gsmg.misp.dao.IPermissionDAO;
import scau.gsmg.misp.dao.IRoleDAO;
import scau.gsmg.misp.dao.IUserDAO;
import scau.gsmg.misp.service.ILoginService;
import scau.gsmg.misp.vo.Permission;
import scau.gsmg.misp.vo.Role;
import scau.gsmg.misp.vo.User;

@Service
public class LoginService implements ILoginService{

	private IUserDAO userDAO;
	private IRoleDAO roleDAO;
	private IPermissionDAO permissionDAO;

	public IUserDAO getUserDAO() {
		return userDAO;
	}

	@Resource
	public void setUserDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}
	public IRoleDAO getRoleDAO() {
		return roleDAO;
	}
	@Resource
	public void setRoleDAO(IRoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}

	public IPermissionDAO getPermissionDAO() {
		return permissionDAO;
	}
	@Resource
	public void setPermissionDAO(IPermissionDAO permissionDAO) {
		this.permissionDAO = permissionDAO;
	}

	@Override
	public User findByLoginName(String loginName) {
		User user=new User();
		user.setName(loginName);
		System.out.println(loginName+"login222");
		return userDAO.userLogin(loginName);//userDao.findUniqueBy("loginName", loginName);
	}
	@Override
	public int num(int number) {
		int c=number+5;
		return c;//userDao.findUniqueBy("loginName", loginName);
	}

	@Override
	public String ok(String name) {
		// TODO Auto-generated method stub
		name=name+"%%%%%%%%%%%%%%%%%%";
		return name;
	}
	
	
	
	
	
	
	@Override
	public User login(User user){
		User u=userDAO.login(user);
		System.out.println("Loginqqqq"+u.getName()+u.getPassword());
		return u;
	}

	@Override
	public List<Role> searchRole(User user) {
		// TODO Auto-generated method stub
		return roleDAO.searchRole(user);
	}

	@Override
	public List<Permission> findPermissions(Integer roleId) {
		// TODO Auto-generated method stub
		return permissionDAO.findPermissions(roleId);
	}
}
