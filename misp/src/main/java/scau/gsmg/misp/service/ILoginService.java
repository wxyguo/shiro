package scau.gsmg.misp.service;

import java.util.List;

import scau.gsmg.misp.vo.Permission;
import scau.gsmg.misp.vo.Role;
import scau.gsmg.misp.vo.User;

public interface ILoginService {
	public User findByLoginName(String loginName);
	public int num(int number);
	public String ok(String name);
	public User login(User user);
	public List<Role> searchRole(User user);
	public List<Permission> findPermissions(Integer roleId);
}
