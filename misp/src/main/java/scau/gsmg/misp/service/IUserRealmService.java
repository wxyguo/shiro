package scau.gsmg.misp.service;

//import javax.naming.AuthenticationException;

//import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;

public interface IUserRealmService  {

	public AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken);// throws AuthenticationException(); 
}
