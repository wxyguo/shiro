package scau.gsmg.misp.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import scau.gsmg.misp.dao.IGoodsTypeDAO;
import scau.gsmg.misp.service.IGoodsTypeService;

/**
 * @Title: GoodsTypeService.java
 * @Group: GaoShenMenGui
 * @Author: WXY
 * @Date: 2015-4-4
 */
@Service
public class GoodsTypeService implements IGoodsTypeService {

	@Resource
	private IGoodsTypeDAO goodsTypeDAO;

	/**
	 * 从第几页获取，每页显示几行获取商品类型信息
	 */
	@Override
	public List getTypeList(String page, String rows) throws Exception {
		// TODO Auto-generated method stub
		return goodsTypeDAO.getTypeList(page, rows);
	}

	/**
	 * 统计商品类型一共有多少数据
	 */
	@Override
	public int getTypeCount() throws Exception {
		// TODO Auto-generated method stub
		return goodsTypeDAO.getTypeCount();
	}

	/**
	 * 增加商品类型
	 */
	@Override
	public boolean addGoodsType(int pid, String name) {
		// TODO Auto-generated method stub
		return goodsTypeDAO.addGoodsType(pid, name);
	}

	/**
	 * 更新商品类型
	 */
	@Override
	public boolean updateGoodsType(int id, String name) {
		// TODO Auto-generated method stub
		return goodsTypeDAO.updateGoodsType(id, name);
	}

	/**
	 * 删除商品类型
	 */
	@Override
	public boolean deleteGoodsType(int id) {
		// TODO Auto-generated method stub
		return goodsTypeDAO.deleteGoodsType(id);
	}

	public IGoodsTypeDAO getGoodsTypeDAO() {
		return goodsTypeDAO;
	}

	public void setGoodsTypeDAO(IGoodsTypeDAO goodsTypeDAO) {
		this.goodsTypeDAO = goodsTypeDAO;
	}

}
