package scau.gsmg.misp.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import scau.gsmg.misp.dao.BaseDAO;
import scau.gsmg.misp.dao.ICustomerDAO;
import scau.gsmg.misp.service.ICustomerService;

/**
 * @Title CustomerService.java
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */

@Service
public class CustomerService extends BaseDAO implements ICustomerService {
	private ICustomerDAO customerDAO;

	public ICustomerDAO getCustomerDAO() {
		return customerDAO;
	}

	@Resource
	public void setCustomerDAO(ICustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	/**
	 * 从第几页获取，每页显示几行获取会员信息
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getCustomerList(String page, String rows, String sort,
			String order) throws Exception {
		// TODO Auto-generated method stub
		return customerDAO.getCustomerList(page, rows, sort, order);
	}

	/**
	 * 统计总的记录数
	 */
	@Override
	public int CountTotalRecords() throws Exception {
		// TODO Auto-generated method stub
		String hql = "from Member m";
		int count = customerDAO.CountRecords(hql, null);
		return count;
	}

	/**
	 * 添加新会员
	 */
	@Override
	public boolean addCustomer(String name, String gender, String cellphone,
			String address, double discount, int integral) throws Exception {
		// TODO Auto-generated method stub
		return customerDAO.addCustomer(name, gender, cellphone, address,
				discount, integral);
	}

	/**
	 * 修改会员信息
	 */
	@Override
	public boolean editCustomer(int id, String name, String gender,
			String cellphone, String address, double discount, int integral)
			throws Exception {
		// TODO Auto-generated method stub
		return customerDAO.editCustomer(id, name, gender, cellphone, address,
				discount, integral);
	}

	/**
	 * 删除会员
	 */
	@Override
	public boolean removeCustomer(int id) throws Exception {
		// TODO Auto-generated method stub
		return customerDAO.removeCustomer(id);
	}

	/**
	 * 搜索会员
	 */
	@Override
	public List searchCustomer(String page, String rows, int id, String name,
			String cellphone, String sort, String order) throws Exception {
		// TODO Auto-generated method stub
		return customerDAO.searchCustomer(page, rows, id, name, cellphone,
				sort, order);
	}

	/**
	 * 通过日期区间搜索会员
	 */
	@Override
	public List searchByDate(Date firstDate, Date lastDate, String page,
			String rows, String sort, String order) throws Exception {
		// TODO Auto-generated method stub
		return customerDAO.searchByDate(firstDate, lastDate, page, rows, sort,
				order);
	}

	/**
	 * 通过日期区间统计记录数
	 */
	@Override
	public int CountRecordsByDateArea(Date firstDate, Date lastDate)
			throws Exception {
		// TODO Auto-generated method stub
		Date[] parameters = new Date[] { firstDate, lastDate };
		String hql = "from Member m where m.createTime between ? and ?";
		int count = customerDAO.CountRecords(hql, parameters);
		return count;
	}

	/**
	 * 统计搜索返回的记录数
	 */
	@Override
	public int CountTotalSearch(int id, String name, String cellphone)
			throws Exception {
		// TODO Auto-generated method stub
		String[] parameters;
		String hql = "from Member m";
		if (id > 0) {
			hql = hql + " where m.id=" + id;
		}
		if (name != null) {
			hql = hql + " where m.name like ?";
		}
		if (cellphone != null) {
			hql = hql + " where m.cellphone=" + cellphone;
		}
		if (name != null) {
			parameters = new String[] { "%" + name + "%" };
		} else {
			parameters = null;
		}
		int count = customerDAO.CountRecords(hql, parameters);
		return count;
	}
}
