package scau.gsmg.misp.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import scau.gsmg.misp.service.IPermissionService;
import scau.gsmg.misp.vo.Permission;

@Service
public class PermissionService implements IPermissionService{

	/**
	 * 获取角色拥有的权限集合
	 * @param userId
	 * @return 结果集合
	 */
	public List<Permission> getPermissions(Integer userId){
		return null;//permissionDao.findPermissions(userId);
	}
	
}
