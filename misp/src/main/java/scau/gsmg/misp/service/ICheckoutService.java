package scau.gsmg.misp.service;

import java.util.List;

/**
 * @Title 收银的所有操作
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月9日
 */
public interface ICheckoutService {
	/**
	 * @Description 收银员通过商品条码搜索商品
	 */
	public List searchGoodsById(String barcode) throws Exception;

	/**
	 * @Description 得到单号,flag为“sp”返回支付单号，flag为“so”返回销售单号
	 */
	public String gainOrder(String flag) throws Exception;

	/**
	 * @Description 挂账
	 */
	public String suspendBill(String rowObj, int customerId, double totalPrice,
			String saleId, int amount, double receivables) throws Exception;

	/**
	 * @Description 提取单据
	 */
	public List gainSuspend() throws Exception;

	/**
	 * @Description 获取单据中的商品
	 */
	public List gainSuspendGoods(int suspendId) throws Exception;

	/**
	 * @Description 删除挂账信息
	 */
	public boolean deleteRecord(int suspendId) throws Exception;

	/**
	 * @Description 销售开单
	 */
	public boolean makeSaleOrder(String rowObj, int customerId,
			double totalPrice, String saleId, int amount, double receivables)
			throws Exception;

	/**
	 * @Description 结算支付
	 */
	public boolean checkout(int cashierId, double totalPrice, double realPay,
			double changes, String saleId, String paymentMethod,
			String paymentId) throws Exception;

	/**
	 * @Description 更新库存
	 */
	public boolean updateInventory(String rowObj) throws Exception;
}
