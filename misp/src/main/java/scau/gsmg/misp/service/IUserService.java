package scau.gsmg.misp.service;

import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;

import scau.gsmg.misp.vo.User;

public interface IUserService {
	/**
	 * @Description 从第几页获取，每页显示几行获取用户信息
	 */
	public List getAllUser(String page, String rows, String sort, String order)
			throws Exception;

	/**
	 * @Description 统计一共有多少行数据
	 */
	public int getUserCount() throws Exception;

	/**
	 * @Description:获取role角色
	 */
	public List userCombobox() throws Exception;

	/**
	 * @Description:增加用户
	 */
	public boolean addUser(User user, int roleId) throws Exception;

	/**
	 * @Description:删除用户
	 */
	public boolean removeUser(int id) throws Exception;

	/**
	 * @Description:修改用户
	 */
	public boolean editUser(int id, int roleId, User user) throws Exception;

	/**
	 * @Description:搜索用户
	 */
	public List searchUser(String page, String rows, int id, String name,
			String sort, String order) throws Exception;

	/**
	 * @Description:搜索用户数量
	 */
	public int searchCount(int id, String name) throws Exception;
	
	
}
