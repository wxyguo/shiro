package scau.gsmg.misp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import scau.gsmg.misp.dao.IGoodsDAO;
import scau.gsmg.misp.service.IGoodsService;
import scau.gsmg.misp.vo.Goods;
import scau.gsmg.misp.vo.GoodsType;

/**
 * @Title: GoodsService.java
 * @Group: GaoShenMenGui
 * @Author: WXY
 * @Date: 2015-4-4
 */
@Service
public class GoodsService implements IGoodsService {

	@Resource
	private IGoodsDAO goodsDAO;

	/**
	 * 对取出商品实现分页功能
	 */
	@Override
	public List getGoodsList(String page, String rows, String sort, String order)
			throws Exception {
		// TODO Auto-generated method stub
		return goodsDAO.getGoodsList(page, rows, sort, order);
	}

	/**
	 * 统计一共有多少数据
	 */
	@Override
	public int getGoodsCount() throws Exception {
		// TODO Auto-generated method stub
		return goodsDAO.getGoodsCount();
	}

	/**
	 * 商品类型树节点Combotree
	 */
	@Override
	public List<GoodsType> combotree() {
		// TODO Auto-generated method stub
		return goodsDAO.combotree();
	}

	/**
	 * @param goods
	 *            对商品的操作
	 */
	/**
	 * 增加商品
	 */
	@Override
	public boolean addGoods(Goods goods) {
		// TODO Auto-generated method stub
		return goodsDAO.addGoods(goods);
	}

	/**
	 * 更新商品 by id
	 */
	@Override
	public boolean updateGoods(Goods goods) {
		// TODO Auto-generated method stub
		return goodsDAO.updateGoods(goods);
	}

	/**
	 * 删除商品
	 */
	@Override
	public boolean removeGoods(int id) {
		// TODO Auto-generated method stub
		return goodsDAO.removeGoods(id);
	}

	/**
	 * 更新商品by name
	 */
	@Override
	public boolean updateGoods(int id, String barcode, String type_id,
			String name, double purchase_price, double price,
			String description, String unit) {
		// TODO Auto-generated method stub
		return goodsDAO.updateGoods(id, barcode, type_id, name, purchase_price,
				price, description, unit);
	}

	/**
	 * @Description:搜索商品
	 */

	@Override
	public List searchGoods(String page, String rows, String barcode,
			String name, String typeName, String sort, String order) {
		// TODO Auto-generated method stub
		return goodsDAO.searchGoods(page, rows, barcode, name, typeName, sort,
				order);
	}

	/**
	 * @Description:搜索出的商品总数
	 */
	@Override
	public int searchCount(String barcode, String name, String typeName) {
		// TODO Auto-generated method stub
		return goodsDAO.searchCount(barcode, name, typeName);
	}

	public IGoodsDAO getGoodsDAO() {
		return goodsDAO;
	}

	public void setGoodsDAO(IGoodsDAO goodsDAO) {
		this.goodsDAO = goodsDAO;
	}
}
