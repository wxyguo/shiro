package scau.gsmg.misp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import scau.gsmg.misp.dao.BaseDAO;
import scau.gsmg.misp.dao.IUserDAO;
import scau.gsmg.misp.service.IUserService;
import scau.gsmg.misp.vo.User;

@Service
public class UserService extends BaseDAO implements IUserService {
	private IUserDAO userDAO;

	public IUserDAO getUserDAO() {
		return userDAO;
	}

	@Resource
	public void setUserDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	/**
	 * 从第几页获取，每页显示几行获取用户信息
	 */
	@Override
	public List getAllUser(String page, String rows, String sort, String order)
			throws Exception {
		// TODO Auto-generated method stub
		return userDAO.getAllUser(page, rows, sort, order);
	}

	/**
	 * 统计一共有多少行数据
	 */
	@Override
	public int getUserCount() throws Exception {
		// TODO Auto-generated method stub
		return userDAO.getUserCount();
	}

	/**
	 * @Description:获取role角色
	 */
	@Override
	public List userCombobox() throws Exception {
		// TODO Auto-generated method stub
		return userDAO.userCombobox();
	}

	/**
	 * @Description:增加用户
	 */
	@Override
	public boolean addUser(User user, int roleId) throws Exception {
		// TODO Auto-generated method stub
		return userDAO.addUser(user, roleId);
	}

	/**
	 * @Description:删除用户
	 */
	@Override
	public boolean removeUser(int id) throws Exception {
		return userDAO.removeUser(id);
	}

	/**
	 * @Description:修改用户
	 */
	@Override
	public boolean editUser(int id, int roleId, User user) throws Exception {
		// TODO Auto-generated method stub
		return userDAO.editUser(id, roleId, user);
	}

	/**
	 * @Description:搜索用户
	 */
	@Override
	public List searchUser(String page, String rows, int id, String name,
			String sort, String order) throws Exception {
		// TODO Auto-generated method stub
		return userDAO.searchUser(page, rows, id, name, sort, order);
	}

	/**
	 * @Description:搜索到的用户数量
	 */
	@Override
	public int searchCount(int id, String name) throws Exception {
		// TODO Auto-generated method stub
		return userDAO.searchCount(id, name);
	}
	
	/**
	 * @param 按登录名查询用户
	 */
	/*@Override
	public User findByLoginName(String loginName) {
		User user=new User();
		user.setName(loginName);
		System.out.println(loginName);
		return userDAO.userLogin(user);//userDao.findUniqueBy("loginName", loginName);
	}*/
	
	/**
	 * 修改用户登录
	 * @param user
	 */
	public void updateUserLogin(User user){
		/*user.setLoginCount((user.getLoginCount()==null?0:user.getLoginCount())+1);
		user.setPreviousVisit(user.getLastVisit());
		user.setLastVisit(DateUtils.getSysTimestamp());
		update(user);*/
	}
}
