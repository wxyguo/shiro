package scau.gsmg.misp.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import scau.gsmg.misp.dao.BaseDAO;
import scau.gsmg.misp.dao.ICheckoutDAO;
import scau.gsmg.misp.dao.impl.CheckoutDAO;
import scau.gsmg.misp.service.ICheckoutService;
import scau.gsmg.misp.vo.Member;
import scau.gsmg.misp.vo.SaleOrder;
import scau.gsmg.misp.vo.User;

/**
 * @Title 收银的所有操作
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月9日
 */
@Service
public class CheckoutService extends BaseDAO implements ICheckoutService {
	private ICheckoutDAO checkoutDAO;
	private static final Logger logger = Logger.getLogger(CheckoutDAO.class);

	public ICheckoutDAO getCheckoutDAO() {
		return checkoutDAO;
	}

	@Resource
	public void setCheckoutDAO(ICheckoutDAO checkoutDAO) {
		this.checkoutDAO = checkoutDAO;
	}

	/**
	 * 收银员通过商品条码搜索商品
	 */
	@Override
	public List searchGoodsById(String barcode) throws Exception {
		// TODO Auto-generated method stub
		return checkoutDAO.searchGoodsById(barcode);
	}

	/**
	 * @Description 得到订单号（销售单或支付单）
	 */
	@Override
	public String gainOrder(String flag) throws Exception {
		// TODO Auto-generated method stub
		String strOrder = "";
		if (flag.equals("so")) {
			strOrder = checkoutDAO.lastSaleRecord();
		}
		if (flag.equals("sp")) {
			strOrder = checkoutDAO.lastPaymentRecord();
		}
		String orderDate = "";// 订单日期
		String orderOrder = "";// 订单顺序
		String orderId = "";// 订单号
		Date nowDate = new Date();// 当前时间
		String formatNowDate = new SimpleDateFormat("yyyyMMdd").format(nowDate);
		if (strOrder != null) {
			String date = strOrder.substring(3, 11);// 获取日期部分
			String order = strOrder.substring(12, 16);// 获取排序部分
			// 判断是不是今天的第一单
			if (formatNowDate.equals(date)) {
				orderDate = formatNowDate;
				int orderOrderInt = Integer.parseInt(order) + 1;
				orderOrder = String.format("%4d", orderOrderInt).replace(" ",
						"0");
			} else {
				orderDate = formatNowDate;
				orderOrder = "0001";
			}
			orderId = orderDate + "-" + orderOrder;// 得到订单号
		} else {
			orderId = formatNowDate + "-" + "0001";
		}
		return flag + "-" + orderId;
	}

	/**
	 * 挂账
	 */
	@Override
	public String suspendBill(String rowObj, int customerId, double totalPrice,
			String saleId, int amount, double receivables) throws Exception {
		// TODO Auto-generated method stub
		String msg = "挂账失败！";
		Member customer = checkoutDAO.gainCustomer(customerId);// 得到会员
		SaleOrder saleOrder = checkoutDAO.createSaleOrder(saleId, customer,
				totalPrice, amount, receivables);
		checkoutDAO.writeGoodsItem(rowObj, saleOrder);
		checkoutDAO.updateState("2", saleId);// 标记为挂账
		msg = "挂账成功，挂账单号：" + saleOrder.getId();
		return msg;
	}

	/**
	 * 提取单据
	 */
	@Override
	public List gainSuspend() throws Exception {
		// TODO Auto-generated method stub
		return checkoutDAO.gainSuspend();
	}

	/**
	 * 获取单据中的商品
	 */
	@Override
	public List gainSuspendGoods(int suspendId) throws Exception {
		// TODO Auto-generated method stub
		return checkoutDAO.gainSuspendGoods(suspendId);
	}

	/**
	 * 删除挂账记录
	 */
	@Override
	public boolean deleteRecord(int suspendId) throws Exception {
		// TODO Auto-generated method stub
		return checkoutDAO.deleteRecord(suspendId);
	}

	/**
	 * 销售开单
	 */
	@Override
	public boolean makeSaleOrder(String rowObj, int customerId,
			double totalPrice, String saleId, int amount, double receivables)
			throws Exception {
		// TODO Auto-generated method stub
		Member customer = checkoutDAO.gainCustomer(customerId);// 得到会员
		SaleOrder saleOrder = checkoutDAO.createSaleOrder(saleId, customer,
				totalPrice, amount, receivables);
		return checkoutDAO.writeGoodsItem(rowObj, saleOrder);
	}

	/**
	 * 结算
	 */
	@Override
	public boolean checkout(int cashierId, double totalPrice, double realPay,
			double changes, String saleId, String paymentMethod,
			String paymentId) throws Exception {
		// TODO Auto-generated method stub
		boolean flag = false;
		try {
			User cashier = checkoutDAO.gainCashier(cashierId);// 得到收银员
			SaleOrder saleOrder = checkoutDAO.gainSaleOrder(saleId);// 得到销售单
			Member customer = saleOrder.getMember();// 得到会员
			// 结算
			if (checkoutDAO.checkout(saleOrder, cashier, paymentId, totalPrice,
					realPay, changes, paymentMethod)) {
				// 把销售单标记为“已支付”，1代表“已支付”
				checkoutDAO.updateState("1", saleId);
				// 如果是会员，则计算积分
				if (customer != null) {
					checkoutDAO.updateIntegral(customer.getId(), saleOrder.getReceivable());// 更新积分
				}
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e);
		}
		return flag;
	}

	/**
	 * 更新库存
	 */
	@Override
	public boolean updateInventory(String rowObj) throws Exception {
		// TODO Auto-generated method stub
		return checkoutDAO.updateInventory(rowObj);
	}

}
