package scau.gsmg.misp.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import scau.gsmg.misp.dao.BaseDAO;
import scau.gsmg.misp.dao.IRoleDAO;
import scau.gsmg.misp.service.IRoleService;
import scau.gsmg.misp.vo.Role;

/**
 * @Title RoleService.java
 * @Group gaoshenmegui
 * @author ZCL
 * @date 2015年4月4日
 */
@Service
public class RoleService extends BaseDAO implements IRoleService {
	private IRoleDAO roleDAO;

	public IRoleDAO getRoleDAO() {
		return roleDAO;
	}

	@Resource
	public void setRoleDAO(IRoleDAO roleDAO) {
		this.roleDAO = roleDAO;
	}

	/**
	 * 获取所有的角色
	 */
	@Override
	public List getAllRole(String page, String rows, String sort, String order)
			throws Exception {
		// TODO Auto-generated method stub
		return roleDAO.getAllRole(page, rows, sort, order);
	}

	/**
	 * 统计记录数
	 */
	@Override
	public int getRoleCount() throws Exception {
		// TODO Auto-generated method stub
		return roleDAO.getRoleCount();
	}

	/**
	 * 添加新角色
	 */
	@Override
	public boolean addRole(Role role) throws Exception {
		return roleDAO.addRole(role);
	}

	/**
	 * 修改角色信息
	 */
	@Override
	public boolean editRole(Role role, int id) throws Exception {
		// TODO Auto-generated method stub
		return roleDAO.editRole(role, id);
	}

	/**
	 * 删除会员
	 */
	@Override
	public boolean removeRole(int id) throws Exception {
		// TODO Auto-generated method stub
		return roleDAO.removeRole(id);
	}

}
