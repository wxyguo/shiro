package scau.gsmg.misp.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * User entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user", catalog = "misp")
public class User implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String password;
	private Date birthday;
	private String gender;
	private String email;
	private String cellphone;
	private Date createTime;
	private Integer count;
	private Date lastVisit;
	private Set<Payment> payments = new HashSet<Payment>(0);
	private Set<UserRole> userRoles = new HashSet<UserRole>(0);
	private Set<ReturnGoods> returnGoodses = new HashSet<ReturnGoods>(0);

	// Constructors

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(String name, String password, String gender, String cellphone,
			Date createTime, Integer count) {
		this.name = name;
		this.password = password;
		this.gender = gender;
		this.cellphone = cellphone;
		this.createTime = createTime;
		this.count = count;
	}

	/** full constructor */
	public User(String name, String password, Date birthday, String gender,
			String email, String cellphone, Date createTime, Integer count,
			Date lastVisit, Set<Payment> payments, Set<UserRole> userRoles,
			Set<ReturnGoods> returnGoodses) {
		this.name = name;
		this.password = password;
		this.birthday = birthday;
		this.gender = gender;
		this.email = email;
		this.cellphone = cellphone;
		this.createTime = createTime;
		this.count = count;
		this.lastVisit = lastVisit;
		this.payments = payments;
		this.userRoles = userRoles;
		this.returnGoodses = returnGoodses;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 32)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "password", nullable = false, length = 32)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birthday", length = 10)
	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "gender", nullable = false, length = 20)
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "email", length = 64)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "cellphone", nullable = false, length = 20)
	public String getCellphone() {
		return this.cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "create_time", nullable = false, length = 10)
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "count", nullable = false)
	public Integer getCount() {
		return this.count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "last_visit", length = 10)
	public Date getLastVisit() {
		return this.lastVisit;
	}

	public void setLastVisit(Date lastVisit) {
		this.lastVisit = lastVisit;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Payment> getPayments() {
		return this.payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	public Set<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	public Set<ReturnGoods> getReturnGoodses() {
		return this.returnGoodses;
	}

	public void setReturnGoodses(Set<ReturnGoods> returnGoodses) {
		this.returnGoodses = returnGoodses;
	}

}