package scau.gsmg.misp.vo;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Member entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "member", catalog = "misp")
public class Member implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String gender;
	private String cellphone;
	private String address;
	private Double discount;
	private Integer integral;
	private Timestamp createTime;
	private Set<SaleOrder> saleOrders = new HashSet<SaleOrder>(0);

	// Constructors

	/** default constructor */
	public Member() {
	}

	/** minimal constructor */
	public Member(String name, String gender, String cellphone,
			Double discount, Timestamp createTime) {
		this.name = name;
		this.gender = gender;
		this.cellphone = cellphone;
		this.discount = discount;
		this.createTime = createTime;
	}

	/** full constructor */
	public Member(String name, String gender, String cellphone, String address,
			Double discount, Integer integral, Timestamp createTime,
			Set<SaleOrder> saleOrders) {
		this.name = name;
		this.gender = gender;
		this.cellphone = cellphone;
		this.address = address;
		this.discount = discount;
		this.integral = integral;
		this.createTime = createTime;
		this.saleOrders = saleOrders;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 20)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "gender", nullable = false, length = 10)
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "cellphone", nullable = false, length = 20)
	public String getCellphone() {
		return this.cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	@Column(name = "address", length = 30)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "discount", nullable = false, precision = 10, scale = 3)
	public Double getDiscount() {
		return this.discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@Column(name = "integral")
	public Integer getIntegral() {
		return this.integral;
	}

	public void setIntegral(Integer integral) {
		this.integral = integral;
	}

	@Column(name = "createTime", nullable = false, length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "member")
	public Set<SaleOrder> getSaleOrders() {
		return this.saleOrders;
	}

	public void setSaleOrders(Set<SaleOrder> saleOrders) {
		this.saleOrders = saleOrders;
	}

}