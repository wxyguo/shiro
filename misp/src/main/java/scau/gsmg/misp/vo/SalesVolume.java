package scau.gsmg.misp.vo;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SalesVolume entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sales_volume", catalog = "misp")
public class SalesVolume implements java.io.Serializable {

	// Fields

	private Integer id;
	private Goods goods;
	private Integer quantity;
	private Date saleDate;

	// Constructors

	/** default constructor */
	public SalesVolume() {
	}

	/** full constructor */
	public SalesVolume(Goods goods, Integer quantity, Date saleDate) {
		this.goods = goods;
		this.quantity = quantity;
		this.saleDate = saleDate;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goods_id", nullable = false)
	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	@Column(name = "quantity", nullable = false)
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "sale_date", nullable = false, length = 10)
	public Date getSaleDate() {
		return this.saleDate;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}

}