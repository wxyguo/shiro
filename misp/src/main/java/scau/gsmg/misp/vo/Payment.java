package scau.gsmg.misp.vo;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Payment entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "payment", catalog = "misp", uniqueConstraints = @UniqueConstraint(columnNames = "payment_id"))
public class Payment implements java.io.Serializable {

	// Fields

	private Integer id;
	private User user;
	private SaleOrder saleOrder;
	private String paymentId;
	private String paymentMethod;
	private Timestamp createTime;
	private Double realPay;
	private Double changes;

	// Constructors

	/** default constructor */
	public Payment() {
	}

	/** minimal constructor */
	public Payment(User user, SaleOrder saleOrder, String paymentId,
			Timestamp createTime, Double realPay, Double changes) {
		this.user = user;
		this.saleOrder = saleOrder;
		this.paymentId = paymentId;
		this.createTime = createTime;
		this.realPay = realPay;
		this.changes = changes;
	}

	/** full constructor */
	public Payment(User user, SaleOrder saleOrder, String paymentId,
			String paymentMethod, Timestamp createTime, Double realPay,
			Double changes) {
		this.user = user;
		this.saleOrder = saleOrder;
		this.paymentId = paymentId;
		this.paymentMethod = paymentMethod;
		this.createTime = createTime;
		this.realPay = realPay;
		this.changes = changes;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sale_order_id", nullable = false)
	public SaleOrder getSaleOrder() {
		return this.saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	@Column(name = "payment_id", unique = true, nullable = false, length = 20)
	public String getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	@Column(name = "payment_method", length = 10)
	public String getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	@Column(name = "create_time", nullable = false, length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "real_pay", nullable = false, precision = 60, scale = 1)
	public Double getRealPay() {
		return this.realPay;
	}

	public void setRealPay(Double realPay) {
		this.realPay = realPay;
	}

	@Column(name = "changes", nullable = false, precision = 60, scale = 1)
	public Double getChanges() {
		return this.changes;
	}

	public void setChanges(Double changes) {
		this.changes = changes;
	}

}