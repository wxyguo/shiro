package scau.gsmg.misp.vo;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ReturnGoods entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "return_goods", catalog = "misp")
public class ReturnGoods implements java.io.Serializable {

	// Fields

	private Integer id;
	private User user;
	private Integer billId;
	private Integer quantity;
	private Date returnDate;
	private Date returnReason;
	private String description;

	// Constructors

	/** default constructor */
	public ReturnGoods() {
	}

	/** minimal constructor */
	public ReturnGoods(User user, Integer billId, Integer quantity,
			Date returnDate, Date returnReason) {
		this.user = user;
		this.billId = billId;
		this.quantity = quantity;
		this.returnDate = returnDate;
		this.returnReason = returnReason;
	}

	/** full constructor */
	public ReturnGoods(User user, Integer billId, Integer quantity,
			Date returnDate, Date returnReason, String description) {
		this.user = user;
		this.billId = billId;
		this.quantity = quantity;
		this.returnDate = returnDate;
		this.returnReason = returnReason;
		this.description = description;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "bill_id", nullable = false)
	public Integer getBillId() {
		return this.billId;
	}

	public void setBillId(Integer billId) {
		this.billId = billId;
	}

	@Column(name = "quantity", nullable = false)
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "return_date", nullable = false, length = 10)
	public Date getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "return_reason", nullable = false, length = 10)
	public Date getReturnReason() {
		return this.returnReason;
	}

	public void setReturnReason(Date returnReason) {
		this.returnReason = returnReason;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}