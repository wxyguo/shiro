package scau.gsmg.misp.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * SaleOrderItems entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sale_order_items", catalog = "misp")
public class SaleOrderItems implements java.io.Serializable {

	// Fields

	private Integer id;
	private SaleOrder saleOrder;
	private Goods goods;
	private Integer quantity;
	private Double price;

	// Constructors

	/** default constructor */
	public SaleOrderItems() {
	}

	/** full constructor */
	public SaleOrderItems(SaleOrder saleOrder, Goods goods, Integer quantity,
			Double price) {
		this.saleOrder = saleOrder;
		this.goods = goods;
		this.quantity = quantity;
		this.price = price;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sale_order_id", nullable = false)
	public SaleOrder getSaleOrder() {
		return this.saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "good_id", nullable = false)
	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	@Column(name = "quantity", nullable = false)
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "price", nullable = false, precision = 22, scale = 0)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}