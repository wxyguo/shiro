package scau.gsmg.misp.vo;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * SaleOrder entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sale_order", catalog = "misp", uniqueConstraints = @UniqueConstraint(columnNames = "sale_order_id"))
public class SaleOrder implements java.io.Serializable {

	// Fields

	private Integer id;
	private Member member;
	private String saleOrderId;
	private Timestamp createTime;
	private String state;
	private Double totalPrice;
	private Integer amount;
	private Double receivable;
	private Set<SaleOrderItems> saleOrderItemses = new HashSet<SaleOrderItems>(
			0);
	private Set<Payment> payments = new HashSet<Payment>(0);

	// Constructors

	/** default constructor */
	public SaleOrder() {
	}

	/** minimal constructor */
	public SaleOrder(String saleOrderId, Timestamp createTime, String state,
			Double totalPrice, Integer amount, Double receivable) {
		this.saleOrderId = saleOrderId;
		this.createTime = createTime;
		this.state = state;
		this.totalPrice = totalPrice;
		this.amount = amount;
		this.receivable = receivable;
	}

	/** full constructor */
	public SaleOrder(Member member, String saleOrderId, Timestamp createTime,
			String state, Double totalPrice, Integer amount, Double receivable,
			Set<SaleOrderItems> saleOrderItemses, Set<Payment> payments) {
		this.member = member;
		this.saleOrderId = saleOrderId;
		this.createTime = createTime;
		this.state = state;
		this.totalPrice = totalPrice;
		this.amount = amount;
		this.receivable = receivable;
		this.saleOrderItemses = saleOrderItemses;
		this.payments = payments;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@Column(name = "sale_order_id", unique = true, nullable = false, length = 60)
	public String getSaleOrderId() {
		return this.saleOrderId;
	}

	public void setSaleOrderId(String saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	@Column(name = "create_time", nullable = false, length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "state", nullable = false, length = 30)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "total_price", nullable = false, precision = 40)
	public Double getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Column(name = "amount", nullable = false)
	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	@Column(name = "receivable", nullable = false, precision = 40, scale = 1)
	public Double getReceivable() {
		return this.receivable;
	}

	public void setReceivable(Double receivable) {
		this.receivable = receivable;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "saleOrder")
	public Set<SaleOrderItems> getSaleOrderItemses() {
		return this.saleOrderItemses;
	}

	public void setSaleOrderItemses(Set<SaleOrderItems> saleOrderItemses) {
		this.saleOrderItemses = saleOrderItemses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "saleOrder")
	public Set<Payment> getPayments() {
		return this.payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}

}