package scau.gsmg.misp.vo;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Goods entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "goods", catalog = "misp", uniqueConstraints = @UniqueConstraint(columnNames = "barcode"))
public class Goods implements java.io.Serializable {

	// Fields

	private Integer id;
	private GoodsType goodsType;
	private String barcode;
	private String name;
	private String unit;
	private String description;
	private Double purchasePrice;
	private Double price;
	private Set<SalesVolume> salesVolumes = new HashSet<SalesVolume>(0);
	private Set<SaleOrderItems> saleOrderItemses = new HashSet<SaleOrderItems>(
			0);
	private Set<Inventory> inventories = new HashSet<Inventory>(0);

	// Constructors

	/** default constructor */
	public Goods() {
	}

	/** minimal constructor */
	public Goods(GoodsType goodsType, String barcode, String name, String unit,
			Double purchasePrice, Double price) {
		this.goodsType = goodsType;
		this.barcode = barcode;
		this.name = name;
		this.unit = unit;
		this.purchasePrice = purchasePrice;
		this.price = price;
	}

	/** full constructor */
	public Goods(GoodsType goodsType, String barcode, String name, String unit,
			String description, Double purchasePrice, Double price,
			Set<SalesVolume> salesVolumes,
			Set<SaleOrderItems> saleOrderItemses, Set<Inventory> inventories) {
		this.goodsType = goodsType;
		this.barcode = barcode;
		this.name = name;
		this.unit = unit;
		this.description = description;
		this.purchasePrice = purchasePrice;
		this.price = price;
		this.salesVolumes = salesVolumes;
		this.saleOrderItemses = saleOrderItemses;
		this.inventories = inventories;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "type_id", nullable = false)
	public GoodsType getGoodsType() {
		return this.goodsType;
	}

	public void setGoodsType(GoodsType goodsType) {
		this.goodsType = goodsType;
	}

	@Column(name = "barcode", unique = true, nullable = false)
	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "unit", nullable = false, length = 20)
	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "purchase_price", nullable = false, precision = 20)
	public Double getPurchasePrice() {
		return this.purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	@Column(name = "price", nullable = false, precision = 20)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "goods")
	public Set<SalesVolume> getSalesVolumes() {
		return this.salesVolumes;
	}

	public void setSalesVolumes(Set<SalesVolume> salesVolumes) {
		this.salesVolumes = salesVolumes;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "goods")
	public Set<SaleOrderItems> getSaleOrderItemses() {
		return this.saleOrderItemses;
	}

	public void setSaleOrderItemses(Set<SaleOrderItems> saleOrderItemses) {
		this.saleOrderItemses = saleOrderItemses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "goods")
	public Set<Inventory> getInventories() {
		return this.inventories;
	}

	public void setInventories(Set<Inventory> inventories) {
		this.inventories = inventories;
	}

}