<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/gray/easyui.css">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/icon.css">
<link rel="stylesheet" href="css/style.css">
<script src="jquery-easyui-1.4.2/jquery.min.js"></script>
<script src="jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
<script src="jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>
<script src="js/index.js"></script>
<script src="js/goods.js"></script>
</head>
<body> 
	<div style="width:100%;height:100%;padding:0px;overflow:hidden">
		<table id="goods_grid"></table>

		<!-- 添加会员的对话框 -->
		<div id="goods_add_dialog" style="width:400px;height: 360px;text-align:right;padding-right:60px;">
			<form id="goods_form" method="post">
				<div style="margin:10px;">
					<label>商品编号</label> <input name="id" data-options="required:false"class="easyui-textbox" style="width:200px;" disabled />
				</div>
				<div style="margin:10px;">
					<label>商品条码</label> <input name="barcode"id="barcode" class="easyui-textbox"style="width:200px;"
					 data-options="required:true,validType:['numberCharacter','maxLength[20]','barcode']" />
				</div>
				<div style="margin:10px;">
					<label>商品类型</label> <input id="type_idTree" name="typeName" style="width:200px;" data-options="required:true">
				</div>
				<div id="hide">
					<input id="tid" name="tid" style="width:150px;" class="easyui-textbox" style="width:200px;"data-options="required:false">
				</div>
				<div style="margin:10px;">
					<label>商品名称</label> <input name="name" data-options="required:true"class="easyui-textbox"style="width:200px;" />
				</div>

				<div style="margin:10px;">
					<label>进货价格</label> <input name="purchasePrice" data-options="required:true"class="easyui-numberbox easyui-textbox" min="0" max="20000" precision="2" style="width:200px;"/>
				</div>
				<div style="margin:10px;">
					<label>出售价格</label> <input name="price" data-options="required:true"class="easyui-numberbox easyui-textbox" min="0" max="20000" precision="2" style="width:200px;"/>
				</div>
				<div style="margin:10px;">
					<label>单位</label> <input name="unit" data-options="required:true"class="easyui-textbox" style="width:200px;" />
				</div>
				<div style="margin:10px;">
					<label>备注</label> <input name="description" data-options="required:false"class="easyui-textbox" style="width:200px;"/>
				</div>


			</form>
		</div>


		<!-- datagrid的工具栏 -->
		<div id="tb_goods" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">
				<div id="tb_goods_add" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</div>
				<div id="tb_goods_edit" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</div>
				<div id="tb_goods_remove" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</div>
				<div id="tb_goods_reload" class="easyui-linkbutton" iconCls="icon-reload" plain="true">刷新</div>
				<!-- 搜索框 -->
				<div id="goods_search"></div>
				<div id="options">
					<div data-options="name:'barcode',iconCls:'icon-ok'">商品条码</div>
					<div data-options="name:'name',iconCls:'icon-ok'">商品名/商品名关键字</div>
					<div data-options="name:'typeName',iconCls:'icon-ok'">商品类型/商品类型关键字</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>