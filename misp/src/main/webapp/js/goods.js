$(function() {

	/* 搜索框 */
	$("#goods_search").searchbox({
		width : 300,
		height : 25,
		menu : '#options',
		prompt : '请输入关键字进行搜索',
		searcher : function(name, value) {
			// 编码，不然传到后台会变成乱码
			name = escape(encodeURIComponent(name));
			url = 'searchGoods.action?' + value + '=' + name;
			// console.log(url);
			loadgrid(url);

		},
	});
	loadgrid('getAllGoods.action'); // 加载datagrid
	function loadgrid(url) {
		$('#goods_grid').datagrid({
			title : '商品信息',
			iconCls : 'icon-edit',
			url : url,
			loadMsg : '正在加载…', // 当从远程站点载入数据时，显示的一条快捷信息
			fit : true,
			nowrap : false, // 设置为true，当数据长度超出列宽时将会自动截取
			fitColumns : true,
			singleSelect : true, // 每次只选中一行
			sortName : 'id', // 默认排序字段
			sortOrder : 'asc', // 升序
			striped : true, // 隔行变色
			pagination : true, // 在底部显示分页工具栏
			pageNumber : 1, // 初始化页码
			pageSize : 10, // 指定每页的大小，服务器要加上page属性和total属性
			pageList : [ 10, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
			rownumbers : true, // 在最前面显示行号
			idField : 'id', // 主键属性
			pagination : true, // 分页
			rownumbers : true, // 行数
			onDblClickCell : onClickCell,
			// 冻结列,当很多咧出现滚动条时该列不会动
			frozenColumns : [ [ {
				title : '商品编号',
				width : '90',
				field : 'id',
				sortable : true
			}, {
				title : '商品条码',
				width : '90',
				field : 'barcode',
				sortable : true
			}, {
				title : '产品类型',
				width : '100',
				field : 'typeName',
				sortable : false,
				editor : 'text'
			}, {
				title : '产品名称',
				width : '100',
				field : 'name',
				sortable : true,
				editor : 'text'
			}, {
				title : '进货价格',
				width : '100',
				field : 'purchasePrice',
				sortable : true,
				editor : {
					type : 'numberbox',
					options : {
						precision : 2
					}
				}
			}, {
				title : '出售价格',
				width : '100',
				field : 'price',
				sortable : true,
				editor : {
					type : 'numberbox',
					options : {
						precision : 2
					}
				}
			}, {
				title : '单位',
				width : '40',
				field : 'unit',
				sortable : true,
				editor : 'text'
			} ] ],
			columns : [ [

			{
				title : '备注',
				width : '200',
				field : 'description',
				sortable : false,
				editor : 'text'
			} ] ],
			// 工具栏按钮
			
			toolbar : '#tb_goods',
		});
		$('.searchbox').css('float', 'right');
		$('.searchbox').css('margin-right', '20px');
	}

	/* begin: 数据表格的单元格编辑 */
	$.extend($.fn.datagrid.methods, {
		editCell : function(jq, param) {
			return jq.each(function() {
				var opts = $(this).datagrid('options');
				var fields = $(this).datagrid('getColumnFields', true).concat(
						$(this).datagrid('getColumnFields'));
				for ( var i = 0; i < fields.length; i++) {
					var col = $(this).datagrid('getColumnOption', fields[i]);
					col.editor1 = col.editor;
					if (fields[i] != param.field) {
						col.editor = null;
					}
				}
				$(this).datagrid('beginEdit', param.index);
				for ( var i = 0; i < fields.length; i++) {
					var col = $(this).datagrid('getColumnOption', fields[i]);
					col.editor = col.editor1;
				}
			});
		}
	});

	var editIndex = undefined;
	function endEditing() {
		if (editIndex == undefined) {
			return true;
		}
		;
		if ($('#goods_grid').datagrid('validateRow', editIndex)) {
			$('#goods_grid').datagrid('endEdit', editIndex);
			editIndex = undefined;
			return true;
		} else {
			return false;
		}
	}
	function onClickCell(index, field) {
		if (endEditing()) {
			$('#goods_grid').datagrid('selectRow', index).datagrid('editCell',
					{
						index : index,
						field : field
					});
			editIndex = index;
		}
	}

	/* end: 数据表格的单元格编辑 */

	/* 添加商品的对话框 */
	$('#goods_add_dialog').dialog({
		closed : true,
	});

	/* 添加新商品 */
	function openDialog(title) {

		$("#goods_add_dialog").dialog({
			resizable : false,
			modal : true,
			buttons : [ { // 设置下方按钮数组
				text : '保存',
				iconCls : 'icon-save',
				handler : function() {
					save();
				}
			}, {
				text : '取消',
				iconCls : 'icon-cancel',
				handler : function() {
					$("#goods_form").form('clear'); // 清空form的数据
					$("#goods_add_dialog").dialog('close');// 关闭goods_add_dialog

				}
			} ]
		});
		$("#goods_add_dialog").dialog('setTitle', title);
		$("#goods_add_dialog").dialog('open');
	}
	function save() {
		$('#goods_form').form('submit', {
			url : url, // 提交地址
			onSubmit : function() {
				return $(this).form('validate'); // 前台字段格式校验
			},
			success : function(result) {
				var result = eval('(' + result + ')');
				if (result.success) {
					$('#goods_grid').datagrid('unselectAll');
					$("#goods_form").form('clear'); // 清空form的数据
					$("#goods_add_dialog").dialog('close');// 关闭goods_add_dialog
					$("#goods_grid").datagrid('reload');// 重新加载
					$.messager.show({ // 显示正确信息
						title : '提示',
						msg : result.message
					});
				} else {
					$('#goods_grid').datagrid('unselectAll');
					$.messager.show({ // 显示错误信息
						title : '错误',
						msg : result.message
					});
				}
			}
		});
	}

	/* 商品的增删改查 */

	/* 新增产品 */
	$("#tb_goods_add").click(function user_add(){
		openDialog('添加新商品');
		$("#type_idTree").combotree(reload, 'combotree.action');
		$("#hide").hide();
		$("#goods_form").form('clear'); // 清空form的数据
		url = 'addGoods.action'; // 后台添加数据action
	});

	/* 修改数据 */
	$("#tb_goods_edit").click(function user_edit(){
		var row = $('#goods_grid').datagrid('getSelected'); // 得到选中的一行数据
		console.info(row);
		if (row == null) {
			$.messager.alert("提示", "请选择一条记录", 'info');
			return;
		}
		openDialog('修改产品信息'); // 显示更新数据dialog窗口
		$("#type_idTree").combotree(reload, 'combotree.action');
		$("#hide").hide();
		$("#goods_form").form('load', row); // 加载选择行数
		url = 'renewGoods.action?id=' + row.id;
	});

	/* 在右边center区域打开菜单，新增tab */
	function openTab(title, url) {
		// 判断是否已存在
		if ($("#tabs").tabs('exists', title)) {
			$('#tabs').tabs('select', title);
		} else {
			// 新增tab
			$('#tabs').tabs('add', {
				title : title,
				closable : true,
				content : createTabContent(url)
			});
		}
	}

	/* 生成tab内容 */
	function createTabContent(url) {
		return '<iframe style="width:100%;height:98%;" scrolling="auto" frameborder="0" src="'
				+ url + '"></iframe>';
	}

	/* 右键菜单关闭事件的处理 */
	function CloseTab(curTab, itemName) {
		// 选中的tab的title
		var curTabTitle = $(curTab).data("tabTitle");
		// 所有tab
		var allTabs = $("#tabs").tabs("tabs");
		// 所有tab的title的数组
		var allTabsTitle = [];
		// 关闭菜单
		if (itemName === "close") {
			$("#tabs").tabs("close", curTabTitle);
			return;
		}
		// 遍历所有tab
		$.each(allTabs, function() {
			var optTab = $(this).panel("options");
			if (optTab.closable && optTab.title != curTabTitle
					&& itemName === "closeother") {
				// 往tab的title数组中添加title
				allTabsTitle.push(optTab.title);
				// 关闭所有
			} else if (optTab.closable && itemName === "closeall") {
				allTabsTitle.push(optTab.title);
			}
		});
		// for循环逐个关闭
		for ( var i = 0; i < allTabsTitle.length; i++) {
			$("#tabs").tabs("close", allTabsTitle[i]);
		}
	}

	/* 定义商品类型下拉框 */
	$('#type_idTree').combotree({
		url : 'combotree.action',
		required : true,
		// 选择树节点触发事件
		onSelect : function(node) {
			// 返回树对象
			var tree = $(this).tree;
			$('#tid').textbox('setValue', node.id);
			// 选中的节点是否为叶子节点,如果不是叶子节点,清除选中
			var isLeaf = tree('isLeaf', node.target);
			if (!isLeaf) {
				// 清除选中
				$('#type_idTree').combotree('clear');
			}
		}
	});

	/* 删除数据 */
	$("#tb_goods_remove").click(function remove(){
		var row = $('#goods_grid').datagrid('getSelected');
		// 如果没有选中记录
		if (row == null) {
			$.messager.alert("提示", "请选择一条记录", 'info');
			return;
		}
		$.messager.confirm('确认', '确定要删除吗？', function(execute) {
			if (execute) {
				$.post('removeGoods.action', {
					id : row.id
				}, function(result) {
					if (result.success) {
						reload();
						$('#goods_grid').datagrid('unselectAll');
						$.messager.show({
							title : '提示',
							msg : result.message
						});
					} else {
						$('#goods_grid').datagrid('unselectAll');
						$.messager.show({
							title : '错误',
							msg : result.message
						});
					}
				}, 'json');
			}
		});

	});
	/* 刷新grid */
	function reload() {
		$('#goods_grid').datagrid('reload');
	}
	/* 点击刷新grid */
	$("#tb_goods_reload").click(function reload() {
		$('#goods_grid').datagrid('reload');
	});
});