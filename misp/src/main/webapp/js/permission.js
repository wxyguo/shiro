$(function(){
	
	loadtTreegrid('laster.json');
	var dg;
	var d;
	var menuDg;
	var menuId=0;
	var parentPermId;
	function loadtTreegrid(url){
		menuDg=$('#permision_grid').treegrid({  
			method: "get",
		    url:url, 
		    fit : true,
			fitColumns : true,
			border : false,
			idField : 'id',
			treeField:'name',
			parentField : 'pid',
			iconCls: 'icon',
			singleSelect : true, // 每次只选中一行
			animate:true, 
			rownumbers:true,
			singleSelect:true,
			striped:true,
		    columns:[[  
                {title : '',
				width : '80',
				field : 'ck',
				sortable : false,
				checkbox : true},
		        {field:'id',title:'id',hidden:true},    
		        {field:'name',title:'名称',width:100},
		        {title:'Task Name',field:'size',width:180},
				{field:'date',title:'Persons',width:60,align:'right'},
				{field:'children',title:'Begin Date',width:80},
		    ]],
		   /* columns:[[    
				        {field:'id',title:'id',hidden:true},    
				        {field:'name',title:'名称',width:100},
				        {title:'Task Name',field:'name',width:180},
						{field:'persons',title:'Persons',width:60,align:'right'},
						{field:'begin',title:'Begin Date',width:80},
						{field:'end',title:'End Date',width:80}
				    ]],*/
//		    enableHeaderClickMenu: false,
//		    enableHeaderContextMenu: false,
//		    enableRowContextMenu: false,
//		    dataPlain: true,
//		    onClickRow:function(rowData){
//		    	menuId=rowData.id;
//		    	parentPermId=rowData.id;
//		    	dg.datagrid('reload',{pid:menuId});
//		    }
		    toolbar : '#tb_permision',
			});
			
		/*	dg=$('#dg').datagrid({   
			method: "get",
			url:url,
		    fit : true,
			fitColumns : true,
			border : false,
			idField : 'id',
			treeField:'name',
			parentField : 'pid',
			iconCls: 'icon',
			animate:true, 
			rownumbers:true,
			singleSelect:true,
			striped:true,
		    columns:[[    
		        {field:'id',title:'id',hidden:true,width:100},    
		        {field:'name',title:'名称',width:100},
		        {field:'url',title:'访问路径',width:100},
		        {field:'permCode',title:'权限编码',width:100},
		        {field:'sort',title:'排序'},
		        {field:'description',title:'描述',width:100}
		    ]],
		    toolbar:'#tb_permision',
		    dataPlain: true
			});*/
			
	}
	
	/* 添加新商品 */
	function openDialog(title) {

		$("#permision_add_dialog").dialog({
			resizable : false,
			modal : true,
			buttons : [ { // 设置下方按钮数组
				text : '保存',
				iconCls : 'icon-save',
				handler : function() {
					save();
				}
			}, {
				text : '取消',
				iconCls : 'icon-cancel',
				handler : function() {
					$("#permision_form").form('clear'); // 清空form的数据
					$("#permision_add_dialog").dialog('close');// 关闭permision_add_dialog

				}
			} ]
		});
		$("#permision_add_dialog").dialog('setTitle', title);
		$("#permision_add_dialog").dialog('open');
	}
	function save() {
		$('#permision_form').form('submit', {
			url : url, // 提交地址
			onSubmit : function() {
				return $(this).form('validate'); // 前台字段格式校验
			},
			success : function(result) {
				var result = eval('(' + result + ')');
				if (result.success) {
					$('#permision_grid').datagrid('unselectAll');
					$("#permision_form").form('clear'); // 清空form的数据
					$("#permision_add_dialog").dialog('close');// 关闭permision_add_dialog
					$("#permision_grid").datagrid('reload');// 重新加载
					$.messager.show({ // 显示正确信息
						title : '提示',
						msg : result.message
					});
				} else {
					$('#permision_grid').datagrid('unselectAll');
					$.messager.show({ // 显示错误信息
						title : '错误',
						msg : result.message
					});
				}
			}
		});
	}

	/* 商品的增删改查 */

	/* 新增产品 */
	$("#tb_permision_add").click(function user_add(){
		openDialog('添加新商品');
		$("#permision_form").form('clear'); // 清空form的数据
		url = 'addGoods.action'; // 后台添加数据action
	});

	/* 修改数据 */
	$("#tb_permision_edit").click(function user_edit(){
		var row = $('#permision_grid').datagrid('getSelected'); // 得到选中的一行数据
		console.info(row);
		if (row == null) {
			$.messager.alert("提示", "请选择一条记录", 'info');
			return;
		}
		openDialog('修改产品信息'); // 显示更新数据dialog窗口
		$("#permision_form").form('load', row); // 加载选择行数
		url = 'renewGoods.action?id=' + row.id;
	});

	/* 在右边center区域打开菜单，新增tab */
	function openTab(title, url) {
		// 判断是否已存在
		if ($("#tabs").tabs('exists', title)) {
			$('#tabs').tabs('select', title);
		} else {
			// 新增tab
			$('#tabs').tabs('add', {
				title : title,
				closable : true,
				content : createTabContent(url)
			});
		}
	}

	/* 生成tab内容 */
	function createTabContent(url) {
		return '<iframe style="width:100%;height:98%;" scrolling="auto" frameborder="0" src="'
				+ url + '"></iframe>';
	}

	/* 右键菜单关闭事件的处理 */
	function CloseTab(curTab, itemName) {
		// 选中的tab的title
		var curTabTitle = $(curTab).data("tabTitle");
		// 所有tab
		var allTabs = $("#tabs").tabs("tabs");
		// 所有tab的title的数组
		var allTabsTitle = [];
		// 关闭菜单
		if (itemName === "close") {
			$("#tabs").tabs("close", curTabTitle);
			return;
		}
		// 遍历所有tab
		$.each(allTabs, function() {
			var optTab = $(this).panel("options");
			if (optTab.closable && optTab.title != curTabTitle
					&& itemName === "closeother") {
				// 往tab的title数组中添加title
				allTabsTitle.push(optTab.title);
				// 关闭所有
			} else if (optTab.closable && itemName === "closeall") {
				allTabsTitle.push(optTab.title);
			}
		});
		// for循环逐个关闭
		for ( var i = 0; i < allTabsTitle.length; i++) {
			$("#tabs").tabs("close", allTabsTitle[i]);
		}
	}


	/* 删除数据 */
	$("#tb_permision_remove").click(function remove(){
		var row = $('#permision_grid').datagrid('getSelected');
		// 如果没有选中记录
		if (row == null) {
			$.messager.alert("提示", "请选择一条记录", 'info');
			return;
		}
		$.messager.confirm('确认', '确定要删除吗？', function(execute) {
			if (execute) {
				$.post('removeGoods.action', {
					id : row.id
				}, function(result) {
					if (result.success) {
						reload();
						$('#permision_grid').datagrid('unselectAll');
						$.messager.show({
							title : '提示',
							msg : result.message
						});
					} else {
						$('#permision_grid').datagrid('unselectAll');
						$.messager.show({
							title : '错误',
							msg : result.message
						});
					}
				}, 'json');
			}
		});

	});
	/* 刷新grid */
	function reload() {
		$('#permision_grid').treegrid('reload');
	}
	/* 点击刷新grid */
	$("#tb_permision_reload").click(function reload() {
		$('#permision_grid').treegrid('reload');
	});
});
	
	
	
//});