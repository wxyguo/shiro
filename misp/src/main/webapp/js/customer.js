$(function(){
	// 判断中文字符 
    jQuery.validator.addMethod("isChinese", function(value, element) {       
         return this.optional(element) || /^[\u0391-\uFFE5]+$/.test(value);       
    }, "请输入中文字符！");
    // 手机号码验证
    jQuery.validator.addMethod("isMobile", function(value, element) {    
        var length = value.length;    
        return this.optional(element) || (length == 11 && /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(value));    
      }, "请正确填写手机号码！");
	
	
    /*表单验证*/
	$("#edit_form").validate({
		rules : {
			name : {
				required : true,
				minlength : 2,
				maxlength:5,
				isChinese:true,
			},
			discount:{
				required : true,
				range:[0,1],
			},
			gender:{
				required : true,
			},
			integral:{
				digits:true,
			},
			cellphone:{
				isMobile:true,
			}
			
		},
		messages : {
			name : {
				required : '姓名不得为空！',
				minlength : '姓名不得小于2 位！',
				maxlength : '姓名不得大于5 位！',
			},
			discount:{
				required : '折扣不能为空！',
				range:'请输入0~1之间的小数！',
			},
			gender:{
				required : '性别不能为空！',
			},
			integral:{
				digits:'请输入整数！',
			},
		}
	});
});