$(function(){
	
	/*window.onbeforeunload = function(){
		
		return "rrrrrrrrrrrrr";
		
	};
	
	function logout(){
		$.ajax({
            type: "POST",
            url: "logout.action",
        });
	}*/
	
	/*初始化登录对话框*/
	$('#login_dialog').dialog({
		width:300,
		title:'登录',
		closed:false,
		top:200,
		resizable: false,
	       modal: true,
	       buttons: [{
	           text: '登录',
	           iconCls: 'icon-ok',
	           handler: function () {
	        	  
	        	   login();
	           }
	       }]
	});
	
	function login(){ 
		 $.ajax({
            type: "POST",
            url: "login.action",
            data: {
            	username:$('#username').textbox('getValue'),
            	password:$('#password').textbox('getValue'),
            	captcha: $('#captcha').textbox('getValue'),//收银员
            },
            dataType: "json",
            success: function(data){
            	if (data.success==true){
            		$('#login_dialog').dialog('close');
            		load_tree();
				} else {
					var meg=data.success;
					 $("#error").text(meg);
				}
            }
        });
	}
	
	
	/*初始化添加会员的对话框*/
	$('#customer_add_dialog').dialog({
		closed:true,
	});
	
	/*初始化修改会员信息对话框*/
	$('#customer_edit_dialog').dialog({
		closed:true,
	});
	
	/**/
	$('#main').layout({
		fit:true, 
	});
	/*导航菜单*/
	$('#navMenu').accordion({
//		fit:true,
		border:false,
		multiple:true,
	});
	/*基础管理树形组件*/
	/*$("#tree").tree({
	   // url : 'treedata.json',
		url:'tree.action',
	    lines : true,
    	onClick : function (node) {
        	if (node.attributes) {
            	openTab(node.text, node.attributes.url);
        	}
    	},
    });*/
	function load_tree(){
		$("#tree").tree({
			   // url : 'treedata.json',
				url:'tree.action',
			    lines : true,
		    	onClick : function (node) {
		        	if (node.attributes) {
		            	openTab(node.text, node.attributes.url);
		        	}
		    	},
		    });
	}
	
	/*前台业务树形组件*/
	$("#checkout_tree").tree({
	    url : 'checkout_treedata.json',
	    lines : true,
    	onClick : function (node) {
        	if (node.attributes) {
            	openTab(node.text, node.attributes.url);
        	}
    	},
    });
    /*选项卡*/
    $("#tabs").tabs({
    	fit:true, 
    	border: false,
    });

	/*绑定tabs的右键菜单*/
	$("#tabs").tabs({
	     onContextMenu : function (e, title) {
             e.preventDefault();
             $('#tabsMenu').menu('show', {
                     left : e.pageX,
                     top : e.pageY
              }).data("tabTitle", title);
	     },
	});

	/*实例化menu的onClick事件*/
	$("#tabsMenu").menu({
	     onClick : function (item) {
	         CloseTab(this, item.name);
	     },
	});
	
	$("#add_gender").combobox({
//		listHeight: 100,
		required:true,
		valueField: 'label',
		textField: 'value',
		value:"男",
		data:[{
		    "label":"男",
		    "value":"男",
		},{
		    "label":"女",
		    "value":"女"
		}]
	});
	
	/*搜索框*/
	$("#search").searchbox({
		width:300,
		height:25,
		menu:'#options',
		prompt:'请输入关键字进行搜索',
		searcher:function(name,value){
//			编码，不然传到后台会变成乱码
			name = escape(encodeURIComponent(name));
			url = 'searchCustomer.action?'+value+'='+name;
//			console.log(url);
			loadgrid(url);
			url='getAllCustomer.action';

		},
	});
	
	
	/*通过日期区间搜索*/
	$(function(){
	    $('#btn_search_date').bind('click', function(){
	    	$('#search_date').form('submit',{
				onSubmit: function(){
					return $(this).form('validate'); //前台字段格式校验
				},
				success: function(){
					var firstDate=($("#firstDate").datebox("getValue"));
					var lastDate=($("#lastDate").datebox("getValue"));
					url = 'searchByDate.action?firstDate='+firstDate+'&lastDate='+lastDate;
//					不知道为什么这样做不行
					/*$('#customer_grid').datagrid('load',{  
				        firstDate:$("#firstDate").datebox("getValue"), 
				        lastDate:$("#lastDate").datebox("getValue"),
				    })*/
					loadgrid(url);
				}
			});
	    });
	});

	
	
	/*在右边center区域打开菜单，新增tab*/
	function openTab(title, url) {
	    //判断是否已存在
	    if ($("#tabs").tabs('exists', title)) {  
	        $('#tabs').tabs('select', title);
	    } else {
	    	//新增tab
		    $('#tabs').tabs('add', {
		        title : title,
		        closable : true,
		        content : createTabContent(url)
		    });
	    }
	}

	/* 生成tab内容 */
	function createTabContent(url){
	    return '<iframe style="width:100%;height:98%;" scrolling="auto" frameborder="0" src="' + url + '"></iframe>';
	}

	/*右键菜单关闭事件的处理*/
	function CloseTab(curTab, itemName) {
	    //选中的tab的title
	    var curTabTitle = $(curTab).data("tabTitle");
	    //所有tab
	    var allTabs = $("#tabs").tabs("tabs");
	    //所有tab的title的数组
	    var allTabsTitle = [];
	    //关闭菜单
	    if (itemName === "close") {
	        $("#tabs").tabs("close", curTabTitle);
	        return;
	    }       
	    //遍历所有tab
		$.each(allTabs, function () {
			var optTab = $(this).panel("options");
			if (optTab.closable && optTab.title != curTabTitle && itemName === "closeother") {
			    //往tab的title数组中添加title
			    allTabsTitle.push(optTab.title);
			//关闭所有
			} else if (optTab.closable && itemName === "closeall") {
			    allTabsTitle.push(optTab.title);
			}
		}); 
	    //for循环逐个关闭
		for (var i = 0; i < allTabsTitle.length; i++) {
			$("#tabs").tabs("close", allTabsTitle[i]);
		}
	}

	var editRow = undefined;
	loadgrid('getAllCustomer.action');
	function loadgrid(url){
        $('#customer_grid').datagrid({
	        title : '会员信息',
	        iconCls: 'icon-edit',
//	        url : 'getAllCustomer.action',
	        url:url,
	        method:'post',
	        loadMsg : '正在加载…',
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true, // 每次只选中一行
	        sortName : 'id', //默认排序字段
	        sortOrder : 'desc',
	        striped : true,  // 隔行变色  
	        pagination : true,  // 分页
	        pageNumber : 1, // 初始化页码 
	        pageSize : 10,  // 指定每页10条记录，服务器要加上page属性和total属性
	        pageList : [ 10, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
	        idField : 'id', // 主键属性
//            rownumbers : true, //显示行号  
            onDblClickCell: function () {
                var row = $("#customer_grid").datagrid('getSelected');
                if (row !=null) {
                    if (editRow != undefined) {
                        $("#customer_grid").datagrid('endEdit', editRow);
                        editRow = undefined;
                    }
                    if (editRow == undefined) {
                        var index = $("#customer_grid").datagrid('getRowIndex', row);
                        $("#customer_grid").datagrid('beginEdit', index);
                        editRow = index;
                    }
                } else {
                	$.messager.alert('警告','未选中商品！','info');
                }
            },//双击可进行单元格编辑
	        frozenColumns : [ [
	            {title : '', width : '80',field : 'ck', sortable : false,checkbox:true},
		        {title : '会员编号', width : '90', field : 'id', sortable : true}, 
		        {title : '姓名', width : '100',    field : 'name',sortable : true, editor:'text'},
		        {title : '性别', width:'80',field:'gender',sortable:true, editor:{type:'combobox',options:{required:true,editable:false,data:[{'id':'男','text':'男'},{'id':'女','text':'女'}],valueField:'id'}}},
		        {title : '折扣', width:'80',field:'discount',sortable:true, editor:{type:'numberbox',options:{precision:2}}},
		        {title : '会员积分', width:'80',field:'integral',sortable:true, editor:'numberbox'},
		        {title : '注册时间', width:'150',field:'createTime',sortable:true}
		        
	        ] ],
	        columns : [ [ 
		        {title : '联系电话', width : '100', field : 'cellphone', sortable : false, editor:'numberbox'}, 
		        {title : '地址', width : '200',    field : 'address', sortable : false, editor:'text'}
	        ] ],
	        toolbar : '#tb'
	        
	    });
        
        $('.searchbox').css('float','right');
        $('.searchbox').css('margin-right','20px');
	}
	

	/*新增会员*/
	$("#tb_add").click(function add(){
		$("#form").form('clear');
		open_add_Dialog('添加新会员'); 
//	         表单清空后默认值就没了，重新设置
	    $('#add_discount').textbox('setValue','0.95');
	    $('#add_integral').textbox('setValue','0');
	    $("#add_gender").combobox('setValue','男');
	    url = 'addCustomer.action';
	    
	});
	/*添加新会员*/
	function open_add_Dialog(title){
	   $("#customer_add_dialog").dialog({
	       resizable: false,
	       modal: true,
	       buttons: [{
	           text: '保存',
	           iconCls: 'icon-save',
	           handler: function () {
	              save_add();
	           }
	       }, {
	           text: '取消',
	           iconCls: 'icon-cancel',
	           handler: function () {
	           	close_add_Dialog();
	           }
	       }]
	   });
	   $("#customer_add_dialog").dialog('setTitle', title);
	   $("#customer_add_dialog").dialog('open');
	}
	
	/* 保存新增会员的数据*/ 
	function save_add(){
		$('#form').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate'); //前台字段格式校验
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success){
					reload();
					close_add_Dialog();	
					$.messager.show({
						title: '提示',
						msg: result.message
					});
				} else {				
					$.messager.show({
						title: '错误',
						msg: result.message
					});
				}
			}
		});
	}
	
	/*关闭添加新会员的窗口*/
	function close_add_Dialog() {  
		$("#form").form('clear');
		$("#customer_add_dialog").dialog('close');
	}
	
	/*修改会员信息对话框*/
	function open_edit_Dialog(title){
	   $("#customer_edit_dialog").dialog({
	       resizable: false,
	       modal: true,
	       buttons: [{
	           text: '保存',
	           iconCls: 'icon-save',
	           handler: function () {
	           	save_edit();
	           }
	       }, {
	           text: '取消',
	           iconCls: 'icon-cancel',
	           handler: function () {
	           	close_edit_Dialog();
	           }
	       }]
	   });
	   $("#customer_edit_dialog").dialog('setTitle', title);
	   $("#customer_edit_dialog").dialog('open');
	}
	
	/* 修改数据*/
	$("#tb_edit").click(function edit() {
	    var row = $('#customer_grid').datagrid('getSelected'); // 得到选中的一行数据
	    //如果没有选中记录
	    if(row == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
	    open_edit_Dialog('修改会员信息');
	    $("#edit_form").form('load', row); // 加载选择行数据
	    url = 'editCustomer.action?id='+row.id; 
	    
	});
	
	/*保存修改会员的数据*/
	function save_edit(){
		$('#edit_form').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate'); //前台字段格式校验
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success){
					reload();
					close_edit_Dialog();
					$.messager.show({
						title: '提示',
						msg: result.message
					});
				} else {				
					$.messager.show({
						title: '错误',
						msg: result.message
					});
				}
			}
		});
	}
	
	/*关闭修改会员的窗口*/
	function close_edit_Dialog() {  
		$("#edit_form").form('clear');
		$("#customer_edit_dialog").dialog('close');
	}
	
	/* 删除数据*/
	$("#tb_remove").click(function remove(){
	    var row = $('#customer_grid').datagrid('getSelected');
	    //如果没有选中记录
	    if(row == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
	    $.messager.confirm('确认', '确定要删除该会员吗？', function (execute) {
	            if (execute) {
	                $.post('removeCustomer.action', { id: row.id }, function (result) { 
	                    if (result.success) {
	                    	
	                        reload();
	                        $.messager.show({
	                            title: '提示',
	                            msg: result.message
	                        });                                 
	                    } else {
	                        $.messager.show({
	                            title: '错误',
	                            msg: result.message
	                    });
	                }
	            }, 'json');
	        }
	    });
	    
	});
	
	
	/* 刷新grid*/
	function reload(){
		$('#customer_grid').datagrid('reload');
	}
	
	/* 点击刷新grid*/
	$("#tb_reload").click(function reload(){
		$('#customer_grid').datagrid('reload');
	});
	
	
	/*单元格编辑的保存，这个功能尚未成功实现*/
	$("#tb_save").click(function save(){
		var row = $('#customer_grid').datagrid('getSelected');
	    //如果没有选中记录
	    if(row == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
	    
	    var change = $("#customer_grid").datagrid('getChanges','updated');
    	if(row!=null){
    		//结束编辑
    		$("#customer_grid").datagrid('endEdit', editRow);
        	$("#checkout_grid").datagrid('unselectAll');
    	}
	    
	    $("#edit_form").form('load', row); // 加载选择行数据
	    url = 'editCustomer.action?id='+row.id;
	    
	    $('#edit_form').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate'); //前台字段格式校验
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success){
					reload();
					close_edit_Dialog();
					$.messager.show({
						title: '提示',
						msg: result.message
					});
				} else {				
					$.messager.show({
						title: '错误',
						msg: result.message
					});
				}
			}
		});
	});
	
	
	
	   

});




