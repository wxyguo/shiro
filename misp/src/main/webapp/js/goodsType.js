$(function(){

	loadgrid(); //加载datagrid
	function loadgrid(){
        $('#goods_type_grid').datagrid({
	        title : '商品类型信息',
	        iconCls: 'icon-edit',
	        url : 'getAllType.action',
	        loadMsg : '正在加载…',  //当从远程站点载入数据时，显示的一条快捷信息
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true, // 每次只选中一行
	        sortName : 'id', //默认排序字段
	        sortOrder : 'asc', // 升序
	        striped : true,  // 隔行变色  
	        pagination : true,  // 在底部显示分页工具栏
	        pageNumber : 1, //初始化页码 
	        pageSize : 10,  // 指定每页的大小，服务器要加上page属性和total属性
	        pageList : [ 10, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
	        rownumbers : true, // 在最前面显示行号 
	        idField : 'id', // 主键属性
	        pagination : true, //分页  
            rownumbers : true, //行数  
	        onClickCell: onClickCell,
	        // 冻结列,当很多咧出现滚动条时该列不会动
	        frozenColumns : [ [
		        {title : '商品类型编号', width : '90', field : 'id', sortable : true},
		        {title : '所属上级类型', width : '90', field : 'pname', sortable : true},
		        {title : '商品类型名称', width : '100',    field : 'name',sortable : true, editor:'text'}
	        ] ],       
	        // 工具栏按钮
	        toolbar: [
				"-", {id: 'add', text: '添加', iconCls: 'icon-add', handler: function () { add();} },
				"-", {id: 'edit', text: '修改', iconCls: 'icon-edit', handler: function () { editType();} }, 
				"-", {id: 'remove', text: '删除',iconCls: 'icon-remove', handler: function () {remove();} }, 
				"-", {id: 'remove',  text: '刷新',iconCls: 'icon-reload', handler: function () {reload();} } 
	        ]
	    });
	}
	
	/* begin:  数据表格的单元格编辑*/
	$.extend($.fn.datagrid.methods, {
		editCell: function(jq,param){
			return jq.each(function(){
				var opts = $(this).datagrid('options');
				var fields = $(this).datagrid('getColumnFields',true).concat($(this).datagrid('getColumnFields'));
				for(var i=0; i<fields.length; i++){
					var col = $(this).datagrid('getColumnOption', fields[i]);
					col.editor1 = col.editor;
					if (fields[i] != param.field){
						col.editor = null;
					}
				}
				$(this).datagrid('beginEdit', param.index);
				for(var i=0; i<fields.length; i++){
					var col = $(this).datagrid('getColumnOption', fields[i]);
					col.editor = col.editor1;
				}
			});
		}
	});
	
	var editIndex = undefined;
	function endEditing(){
		if (editIndex == undefined){return true;};
		if ($('#goods_type_grid').datagrid('validateRow', editIndex)){
			$('#goods_type_grid').datagrid('endEdit', editIndex);
			editIndex = undefined;
			return true;
		} else {
			return false;
		}
	}
	function onClickCell(index, field){
		if (endEditing()){
			$('#goods_type_grid').datagrid('selectRow', index)
					.datagrid('editCell', {index:index,field:field});
			editIndex = index;
		}
	}

/* end:  数据表格的单元格编辑*/
	
	
	/*添加商品类型的对话框*/
	$('#type_add_dialog').dialog({
		closed:true,
	});

	/*添加商品类型的对话框*/
	$('#type_edit_dialog').dialog({
		closed:true,
	});

	/*添加新产商品型*/
	 function openDialog(title){
		 
        $("#type_add_dialog").dialog({
        	
            resizable: false,
            modal: true,
            buttons: [{ //设置下方按钮数组
                text: '保存',
                iconCls: 'icon-save',
                handler: function () {
                   save();
                }
            }, {
                text: '取消',
                iconCls: 'icon-cancel',
                handler: function () {
                 
                	 $("#type_form").form('clear'); // 清空form的数据
                     $("#type_add_dialog").dialog('close');// 关闭goods_add_dialog

                }
            }]
        });
        $("#type_add_dialog").dialog('setTitle', title);
        $("#type_add_dialog").dialog('open');
    }
	
	 function save(){
		 $('#type_form').form('submit',{
	         url: url,  //提交地址
	         onSubmit: function(){
	             return $(this).form('validate'); //前台字段格式校验
	         },
	         success: function(result){
	             var result = eval('('+result+')');
	             if (result.success){
	            	 $("#type_form").form('clear'); // 清空form的数据
	            	 $("#type_add_dialog").dialog('close');// 关闭goods_add_dialog    
	                 $("#goods_type_grid").datagrid('reload');// 重新加载
	                 $("#pname").combotree(reload,'combotree.action');
	                 $('#goods_type_grid').datagrid('unselectAll');
	                 $.messager.show({    //显示正确信息
	                     title: '提示',
	                     msg: result.message
	                 });
	             } else {     
	            	 $('#goods_type_grid').datagrid('unselectAll');
	                 $.messager.show({   //显示错误信息
	                     title: '错误',
	                     msg: result.message
	                 });
	             }
	         }
	     }); 
	 }
	 
	 /*修改新商品类型editDialog*/
	 function editDialog(title){
		 
        $("#type_edit_dialog").dialog({
        	
            resizable: false,
            modal: true,
            buttons: [{ //设置下方按钮数组
                text: '保存',
                iconCls: 'icon-save',
                handler: function () {
                	saveEdit();
                }
            }, {
                text: '取消',
                iconCls: 'icon-cancel',
                handler: function () {
                 
                	 $("#type_edit_form").form('clear'); // 清空form的数据
                     $("#type_edit_dialog").dialog('close');// 关闭goods_add_dialog

                }
            }]
        });
        $("#type_edit_dialog").dialog('setTitle', title);
        $("#type_edit_dialog").dialog('open');
    }
	 function saveEdit(){
		 $('#type_edit_form').form('submit',{
	         url: url,  //提交地址
	         onSubmit: function(){
	             return $(this).form('validate'); //前台字段格式校验
	         },
	         success: function(result){
	             var result = eval('('+result+')');
	             if (result.success){
	            	 $("#type_edit_form").form('clear'); // 清空form的数据
	            	 $("#type_edit_dialog").dialog('close');// 关闭goods_add_dialog    
	                 $("#goods_type_grid").datagrid('reload');// 重新加载
	                 $("#pname").combotree(reload,'combotree.action');
	                 $('#goods_type_grid').datagrid('unselectAll');
	                 $.messager.show({    //显示正确信息
	                     title: '提示',
	                     msg: result.message
	                 });
	             } else {
	            	 $('#goods_type_grid').datagrid('unselectAll');
	                 $.messager.show({   //显示错误信息
	                     title: '错误',
	                     msg: result.message
	                 });
	             }
	         }
	     }); 
	 }
	 /*商品类型的增删改查*/

		/*新增商品类型*/
		function add(){
			check();
			openDialog('添加新商品类型'); 
			$("#hide").hide();
	        $("#type_form").form('clear'); // 清空form的数据
	        url = 'addGoodsType.action'; //后台添加数据action
		}

		/* 修改数据*/
	    function editType() {
	        var row = $('#goods_type_grid').datagrid('getSelected'); // 得到选中的一行数据
	        //如果没有选中记录
	        if(row == null){
	            $.messager.alert("提示", "请选择一条记录",'info');
	            return;
	        }
	        editDialog('修改商品类型信息'); // 显示更新数据dialog窗口
	        $("#type_edit_form").form('load', row); // 加载选择行数据
	        url = 'updateGoodsType.action?id='+row.id; 
	    }
	
	    /*定义商品类型下拉框*/
	    $('#pname').combotree({
 	        url: 'combotree.action',
 	        required: true,
 	       //选择树节点触发事件  
 	        onSelect : function(node) {  
 	       //返回树对象  
 	        var tree = $(this).tree;  
 	        $('#pbid').textbox('setValue',node.id);
 	      //选中的节点是否为叶子节点,如果不是叶子节点,清除选中  
 	        var isLeaf = tree('isLeaf', node.target);  
 	        if (!isLeaf) {  
 	         //清除选中  
 	            $('#type_idTree').combotree('clear');  
 	        }  
 	     }  
 	    });
	   
	  /*通过radio设置有无上级属性类型*/
	    function check(){
	    	 $(document).ready(function() {
	    		$('#pname').combo('disable');
	    		$('#name').textbox('disable');
		    	$("#radno").attr("checked",true);
		    	$('#radyes').click(function(){
		    		$('#pname').combo('enable');
		    		$('#name').textbox('enable');
		    	});
	            $('#radno').click(function(){
	            	// $("#type_form").form('clear'); // 清空form的数据
	            	 $('#radno').attr('checked',true);
	            	$('#pname').combo('disable');
	            	$('#name').textbox('enable');
		    	});
		    });
	    }
	 /*在右边center区域打开菜单，新增tab*/
		function openTab(title, url) {
		    //判断是否已存在
		    if ($("#tabs").tabs('exists', title)) {  
		        $('#tabs').tabs('select', title);
		    } else {
		    //新增tab
			    $('#tabs').tabs('add', {
			        title : title,
			        closable : true,
			        content : createTabContent(url)
			    });
		    }
		}

		/* 生成tab内容 */
		function createTabContent(url){
		    return '<iframe style="width:100%;height:98%;" scrolling="auto" frameborder="0" src="' + url + '"></iframe>';
		}

		/*右键菜单关闭事件的处理*/
		function CloseTab(curTab, itemName) {
		    //选中的tab的title
		    var curTabTitle = $(curTab).data("tabTitle");
		    //所有tab
		    var allTabs = $("#tabs").tabs("tabs");
		    //所有tab的title的数组
		    var allTabsTitle = [];
		    //关闭菜单
		    if (itemName === "close") {
		        $("#tabs").tabs("close", curTabTitle);
		        return;
		    }       
		    //遍历所有tab
			$.each(allTabs, function () {
				var optTab = $(this).panel("options");
				if (optTab.closable && optTab.title != curTabTitle && itemName === "closeother") {
				    //往tab的title数组中添加title
				    allTabsTitle.push(optTab.title);
				//关闭所有
				} else if (optTab.closable && itemName === "closeall") {
				    allTabsTitle.push(optTab.title);
				}
			}); 
		    //for循环逐个关闭
			for (var i = 0; i < allTabsTitle.length; i++) {
				$("#tabs").tabs("close", allTabsTitle[i]);
			}
		}

    /* 删除商品类型*/
    function remove(){
        var row = $('#goods_type_grid').datagrid('getSelected');
        //如果没有选中记录
        if(row == null){
            $.messager.alert("提示", "请选择一条记录",'info');
            return;
        }
        $.messager.confirm('确认', '确定要删除吗？', function (execute) {
                if (execute) {
                	
                    $.post('deleteGoodsType.action', { id: row.id }, function (result) { 
                        if (result.success) {
                            reload();
                            $("#pname").combotree(reload,'combotree.action');
                            $('#goods_type_grid').datagrid('unselectAll');
                          //  delete row;
                            $.messager.show({
                                title: '提示',
                                msg: result.message
                            });                                 
                        } else {
                        	$('#goods_type_grid').datagrid('unselectAll');
                            $.messager.show({
                                title: '错误',
                                msg: result.message
                            });
                        }
                    }, 'json');
                }
            });
         
    }
    /* 刷新grid*/
	function reload(){
		$('#goods_type_grid').datagrid('reload');
	}
});