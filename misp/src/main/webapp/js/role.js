$(function(){
	/*数据表格*/
	loadgrid('getAllRole.action');
	function loadgrid(url){
        $('#role_grid').datagrid({
	        title : '用户角色信息',
	        iconCls: 'icon-edit',
	        url:url,
	        method:'post',
	        loadMsg : '正在加载…',
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true,
	        sortName : 'id',
	        sortOrder : 'asc',
	        striped : true,  // 隔行变色  
	        pagination : true,
	        pageNumber : 1, // 初始化页码 
	        pageSize : 10,  // 每页10条记录
	        pageList : [ 10, 20, 30, 50 ],
	        idField : 'role.id', // 主键属性
            rownumbers : true,
//            onDblClickCell: onClickCell,//双击可进行单元格编辑
	        frozenColumns : [ [
	                           
		        {title : '角色编号', width : '90', field : 'role.id', sortable : false}, 
		        {title : '角色名称', width : '100',    field : 'role.name',sortable : false, editor:'text'},
		        {title : '角色代码', width:'80',field:'role.roleCode',sortable :false, editor:'text'}
		        
	        ] ],
	        columns : [ [ 
	            {title : '描述', width:'300',field:'role.description',sortable:false, editor:'text'}
	     	        ] ],
	       /* toolbar: [
				"-", {id: 'add', text: '添加', iconCls: 'icon-add', handler: function () { add_role();} },
				"-", {id: 'edit', text: '修改', iconCls: 'icon-edit', handler: function () { edit_role();} }, 
				"-", {id: 'remove', text: '删除',iconCls: 'icon-remove', handler: function () {remove_role();} }, 
				"-", {id: 'reload',  text: '刷新',iconCls: 'icon-reload', handler: function () {reload_role();} },
	        ]*/
        toolbar : '#tb_role',
	    });
	}
	
	/*初始化角色的对话框*/
	$('#role_dialog').dialog({
		closed:true,
	});
	
	/*添加角色*/
	$("#tb_role_add").click(function add_role(){
		 open_role_Dialog('添加新角色');
//		 隐藏id的输入框
		 $("#role_id_div").hide();
		 $("#role_form").form('clear');
		 url = 'addRole.action';
		    
	 }
	);
	
	/*修改角色信息*/
	$("#tb_role_update").click(function edit_role() {
	    var row = $('#role_grid').datagrid('getSelected'); // 得到选中的一行数据
	    //如果没有选中记录
	    if(row == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
	    open_role_Dialog('编辑角色信息');
	    $("#role_form").form('load', row); // 加载选择行数据
	    var id=$("#role_id").textbox('getText'); 
	    url = 'editRole.action?id='+id; 
	    
	}
	);
	
	/*打开添加角色对话框*/
	 function open_role_Dialog(title){
       $("#role_dialog").dialog({
           resizable: false,
           modal: true,
           buttons: [{
               text: '保存',
               iconCls: 'icon-save',
               handler: function () {
                  save_role();//提交数据
               }
           }, {
               text: '取消',
               iconCls: 'icon-cancel',
               handler: function () {
               	close_role_Dialog();
               }
           }]
       });
       $("#role_dialog").dialog('setTitle', title);
       $("#role_dialog").dialog('open');
   }
	 
	 /*保存角色信息*/
	 function save_role(){
		$('#role_form').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate'); //前台字段格式校验
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success){
					reload_role();
					close_role_Dialog();	
					$.messager.show({
						title: '提示',
						msg: result.message
					});
				} else {				
					$.messager.show({
						title: '错误',
						msg: result.message
					});
				}
			}
		});
	}
	 
	/*刷新*/
	 $("#tb_role_reload").click(function reload_role(){
		$('#role_grid').datagrid('reload');
	});
	
	/*关闭编辑角色的窗口*/
	function close_role_Dialog() {  
		$("#role_form").form('clear');
		$("#role_dialog").dialog('close');
	}
	
	/* 删除数据*/
	$("#tb_role_delete").click(function remove_role(){
	    var row = $('#role_grid').datagrid('getSelected');
	    //如果没有选中记录
	    if(row == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
	    $.messager.confirm('确认', '删除角色同时会将属于该角色的所有用户设为未分配角色，是否删除？', function (execute) {
	            if (execute) {
	            	$("#role_form").form('load', row);
	            	var id=$("#role_id").textbox('getValue');
	                $.post('removeRole.action', { id: id }, function (result) { 
	                    if (result.success) {
	                        reload_role();
	                        close_role_Dialog();
	                        $.messager.show({
	                            title: '提示',
	                            msg: result.message
	                        });                                 
	                    } else {
	                        $.messager.show({
	                            title: '错误',
	                            msg: result.message
	                    });
	                }
	            }, 'json');
	        }
	    });
	    
	});
	
	
	
});