$(function() {

	/* 初始化用户的对话框 */
	$('#user_dialog').dialog({
		closed : true,
	});

	/* 下拉框 */
	$("#user_gender").combobox({
		required : true,
		valueField : 'label',
		textField : 'value',
		value : "男",
		data : [ {
			"label" : "男",
			"value" : "男"
		}, {
			"label" : "女",
			"value" : "女"
		} ]
	});

	/* 搜索框 */
	$("#user_search").searchbox({
		width : 300,
		height : 25,
		menu : '#options',
		prompt : '请输入关键字进行搜索',
		searcher : function(name, value) {
			// 编码，不然传到后台会变成乱码
			name = escape(encodeURIComponent(name));
			url = 'searchUser.action?' + value + '=' + name;
		//	console.log(url);
			loadgrid(url);

		},
	});

	/* 数据表格 */
	loadgrid('getAllUser.action');
	function loadgrid(url) {
		$('#user_grid').datagrid({
			title : '用户信息',
			iconCls : 'icon-edit',
			url : url,
			method : 'post',
			loadMsg : '正在加载…',
			fit : true,
			nowrap : false, // 设置为true，当数据长度超出列宽时将会自动截取
			fitColumns : true,
			singleSelect : true,
			sortName : 'id',
			sortOrder : 'desc',
			striped : true, // 隔行变色
			pagination : true,
			pageNumber : 1, // 初始化页码
			pageSize : 10, // 指定每页10条记录
			pageList : [ 10, 20, 30, 50 ],
			idField : 'id', // 主键属性
			// rownumbers : true,
			// onDblClickCell: onClickCell,//双击可进行单元格编辑
			frozenColumns : [ [ {
				title : '',
				width : '80',
				field : 'ck',
				sortable : false,
				checkbox : true
			}, {
				title : '用户编号',
				width : '80',
				field : 'id',
				sortable : true
			}, {
				title : '姓名',
				width : '100',
				field : 'user.name',
				sortable : true,
				editor : 'text'
			}, {
				title : '用户类型',
				width : '100',
				field : 'roleName',
				sortable : true,
				editor : 'text'
			}, {
				title : '性别',
				width : '80',
				field : 'user.gender',
				sortable : true,
				editor : 'text'
			}, {
				title : '生日',
				width : '100',
				field : 'user.birthday',
				sortable : true
			}, {
				title : 'email',
				width : '150',
				field : 'user.email',
				sortable : false
			}, {
				title : '联系电话',
				width : '120',
				field : 'user.cellphone',
				sortable : false,
				editor : 'numberbox'
			}, {
				title : '注册时间',
				width : '100',
				field : 'createTime',
				sortable : false
			}, {
				title : '登录次数',
				width : '80',
				field : 'count',
				sortable : true,
				editor : 'numberbox'
			}, {
				title : '最后一次登录时间',
				width : '120',
				field : 'lastVisit',
				sortable : true,
				editor : 'text'
			}, ] ],
			toolbar : '#tb_user'
		});
		$('.searchbox').css('float', 'right');
		$('.searchbox').css('margin-right', '20px');
	}

	/* 新增用户 */
	$("#tb_user_add").click(function user_add() {
		$("#user_form").form('clear');
		user_Dialog('添加新用户');
		$("#hide").hide();
		url = 'addUser.action';

	});

	$('#user_role').combobox({
		url : 'userCombobox.action',
		valueField : 'id',
		textField : 'text',
		onSelect : function(record) {
			$('#roleId').textbox('setValue', record.id);
		}
	});

	/* 添加新用户 */
	function user_Dialog(title) {
		$("#user_dialog").dialog({
			resizable : false,
			modal : true,
			buttons : [ {
				text : '保存',
				iconCls : 'icon-save',
				handler : function() {
					user_save();
				}
			}, {
				text : '取消',
				iconCls : 'icon-cancel',
				handler : function() {
					close_user_Dialog();
				}
			} ]
		});
		$("#user_dialog").dialog('setTitle', title);
		$("#user_dialog").dialog('open');
	}

	/* 保存用户的数据 */
	function user_save() {
		$('#user_form').form('submit', {
			url : url,
			onSubmit : function() {
				return $(this).form('validate'); // 前台字段格式校验
			},
			success : function(result) {
				var result = eval('(' + result + ')');
				if (result.success) {
					user_reload();
					close_user_Dialog();
					$('#user_grid').datagrid('unselectAll');
					$.messager.show({
						title : '提示',
						msg : result.message
					});
				} else {
					$('#user_grid').datagrid('unselectAll');
					$.messager.show({
						title : '错误',
						msg : result.message
					});
				}
			}
		});
	}

	/* 关闭窗口 */
	function close_user_Dialog() {
		$("#user_form").form('clear');
		$("#user_dialog").dialog('close');
	}

	/* 修改数据 */
	$("#tb_user_edit").click(function user_edit() {
		var row = $('#user_grid').datagrid('getSelected'); // 得到选中的一行数据
		// 如果没有选中记录
		if (row == null) {
			$.messager.alert("提示", "请选择一条记录", 'info');
			return;
		}
		user_Dialog('修改用户信息');
		$("#hide").hide();
		$("#user_form").form('load', row); // 加载选择行数据
		url = 'editUser.action?id=' + row.id;
	});

	/* 刷新grid */
	function user_reload() {
		$('#user_grid').datagrid('reload');
	}

	/* 点击刷新grid */
	$("#tb_user_reload").click(function reload() {
		$('#user_grid').datagrid('reload');
	});

	/* 删除数据 */
	$("#tb_user_remove").click(function remove() {
		var row = $('#user_grid').datagrid('getSelected');
		// 如果没有选中记录
		if (row == null) {
			$.messager.alert("提示", "请选择一条记录", 'info');
			return;
		}
		$.messager.confirm('确认', '确定要删除该用户吗？', function(execute) {
			if (execute) {
				$.post('removeUser.action', {
					id : row.id
				}, function(result) {
					if (result.success) {
						user_reload();
						close_user_Dialog();
						$('#user_grid').datagrid('unselectAll');
						$.messager.show({
							title : '提示',
							msg : result.message
						});
					} else {
						$('#user_grid').datagrid('unselectAll');
						$.messager.show({
							title : '错误',
							msg : result.message
						});
					}
				}, 'json');
			}
		});

	});

});