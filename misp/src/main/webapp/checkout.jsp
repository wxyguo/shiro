<!-- 收银页面 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'checkout.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/gray/easyui.css">
	<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/icon.css">
	<script src="jquery-easyui-1.4.2/jquery.min.js"></script>
	<script src="jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
	<script src="jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>
	<script src="js/checkout.js"></script>
	<style type="text/css">
		/*表格行高*/
		.datagrid-row {
	        height: 42px;/* 本来是34 */
	    }
	    #select_customer .datagrid-row {
	        height: 30px;
	    }
	    #select_goods .datagrid-row {
	        height: 30px;
	    }
	    .quick {
	    	background-color:#ccc;
	    	margin:3px;
	    	padding:7px 0 0 3px;
	    	float:left;
	    	height:50px;
	    	width:61px;
	    	border-radius:5px;
			-moz-border-radius:5px;
			font-size:12px;
			cursor:pointer;
	    }
	    fieldset {
	    	border-radius:3px;
	    }
	    input {
	    	font-family:Microsoft YaHei;
	    }
	    /*表格行样式*/
        .datagrid-row td div {
        	font-size:20px;/* 本来是18 */
        	font-family:Microsoft YaHei;
        	color:black;
        }
        #select_goods .datagrid-row td div,#select_customer .datagrid-row td div,#get_suspend_order .datagrid-row td div{
        	font-size:12px;
        }
        #receipt_dialog .datagrid-row td div{
        	font-size:12px;
        }
	</style>
  </head>
  
  <body style="margin-bottom:0;font-family:Microsoft YaHei;">
    <div style="width:100%;height:100%;padding:0;overflow:hidden;color:#333;">
	    <div style="font-family:Microsoft YaHei;margin:0;padding-bottom:8px;font-size:14px;">
	    	
		    	<fieldset id="checkout_head" style="padding:6px;margin:0;">
		    		<div style="float:left;margin:0;padding-right:13px;">
		    			<span>销售单号:</span>
		    			<input id="orderid" readonly
		    				style="font-size:18px;width:180px;height:25px;border:none;" />
		    		</div>
			    	<form action="" method="post" id="customer_details" style="margin:0;padding:0;">	
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员号:</span>
			    			<input id="customerId" name="id" class="easyui-textbox" data-options="prompt:'快捷键ctrl+→',editable:false"
			    				style="width:120px;height:25px;" />
			    		</div>
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员姓名:</span>
			    			<input readonly id="customerName" name="name" class="easyui-textbox" 
			    				style="width:80px;height:25px;font-weight:bold;" />
			    		</div>
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员积分:</span>
			    			<input readonly id="customerIntegral" class="easyui-textbox" name="integral"  
			    				style="width:80px;height:25px;font_weight:bold;" />
			    		</div>
			    		<input type="hidden" name="discount" id="hidden_discount" />
		    		</form>
		    		<div style="float:left;margin:0;padding-right:10px;">
		    			<span>收银员:</span>
		    			<input readonly class="easyui-textbox" value='陆希' id="cashier"
		    				style="width:80px;height:25px;" />
		    			<input type="hidden" value="5" id="cashierId">
		    		</div>
		    		
		    		<div id="time" style="float:right;font-family:'Verdana';font-size:14px;"></div>
		    	
		    	</fieldset>
	    	
	    </div>	
	    
    	<div style="height:410px;width:100%;margin-top:0;" id="checkout_grid_div">
	    	<!-- 表格 -->
			<table id="checkout_grid"></table>
		</div>
		
		<div style="font-family:Microsoft YaHei;height:100%;font-size:14px;">
			<fieldset style="float:left;height:142px;width:260px;">
				<legend>输入框</legend>
				<div style="margin:7px 0;font-weight:bold;">请输入商品条码（回车键结算）</div>
				<form action="" method="post" id="goods_input_form">
					<input name="goods.barcode" type="text" id="goods_input" 
						style="border: 1px #666 solid;border-radius:5px;height:60px;width:240px;font-size:36px;">
				</form>
				<div style="padding:0;">* F2：输入框获取焦点</div>
			</fieldset>
			
			<fieldset style="float:left;height:142px;width:560px;">
				<legend>账单信息</legend>
				<div style="margin:0;padding:0;width:225px;font-size:18px;float:left;">
					<div style="float:right;margin-top:0;">
						<span>合计数量:</span>
						<input type="text" readonly value="0" id="amount" 
							style="border:none;width:140px;font-size:30px;">
					</div>
					<div style="margin-top:3px;float:right">
						<span>合计金额:</span>
						<input type="text" readonly id="totalPrice"  value="0.0" 
							style="border:none;width:140px;font-size:30px;">
					</div>
					<div style="margin-top:3px;float:right">
						<span>整单打折:</span>
						<input type="text" readonly name="discount"  value="" id="discount" 
							style="border:none;width:140px;font-size:30px;">
					</div>
				</div>
					
				<div style="margin:5px;font-size:22px;float:left;">
					<div style="color:black;padding-left:10px;">应收金额:</div>
					<input type="text" readonly id="receivable"  value="0.0" 
						style="border:none;width:320px;font-size:70px;">
				</div>
				
			</fieldset>
			
			<fieldset id="" style="float:left;height:142px;width:230px;">
				<legend>快捷功能</legend>
				<div class="quick" id="search_good">查找商品(F4)</div>
				<div class="quick" id="add_customer">注册新会员</div>
				<div class="quick" id="reset">清空单据</div>
				<div class="quick" id="suspend_order">挂账</div>
				<div class="quick" id="collect_order">提取单据</div>
				<div class="quick" id="regain_bill">重打小票</div>
				
			</fieldset>
		</div>
		
    </div>
    
    <!--  结算窗口 -->
    <div class="easyui-dialog" id="checkout" style="color:black;font-size:36px;padding-right:40px;width:560px;height: 520px;text-align:right">
    	<form action="" id="checkout_form">
    		<div style="padding-top:30px;margin:15px;">
	    		<span>单号:</span>
	    		<input id="checkout_id" readonly style="border:none;width:280px;height:50px;font-size:30px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>应收金额:</span>
	    		<input id="receivables" value="0.0" readonly style="border:none;width:280px;height:50px;font-size:50px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>实收金额:</span>
	    		<input id="accounts" style="border: 1px #ccc solid;border-radius:5px;width:280px;height:55px;font-size:50px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>找款:</span>
	    		<input id="change" readonly value="0.0" style="border:none;color:red;font-weight:bold;width:280px;height:50px;font-size:50px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>结算方式:</span>
	    		<select  id="checkout_way" style="border: 1px #ccc solid;border-radius:5px;width:280px;height:50px;font-size:28px;">
	    			<option value="0">现金支付</option>
	    			<option value="1">银行卡支付</option>
	    		</select>
	    	</div>
	    	<div style="font-size:14px;height:15px;margin-right:5px;">
	    		<input type="checkbox" checked="checked" name="outputReceipt" id="outputReceipt" />打印小票
	    	</div>
	    	
	    	<!-- <button id="submit_checkout">结算</button>
	    	<button id="close_checkout">取消</button> -->
    	</form>
    </div>
    
    <!-- 小票 -->
    <div id="receipt_dialog" style="font-size:14px;padding:0 5px;">
    	<div style="text-align:center;font-size:18px;margin:6px;">Dainty-Bits</div>
    	<div>流水号：<span id="t_paymentId"></span></div>
    	<div>收银员：<span id="t_cashier"></span></div>
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<!-- <table id="receipt_grid" class="easyui-datagrid"></table> -->
    	<div>
    		<table id="receipt_table" style="font-size:14px;"></table>
    	</div>
    	
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<table style="font-size:14px;">
    		<tr>
    			<td><div>会员：<span id="t_customer"></span></div></td>
    			<td><div>数量：<span id="t_amount"></span></div></td>
    		</tr>
    		<tr>
    			<td><div style="width:150px;">折扣：<span id="t_discount"></span></div></td>
    			<td><div>合计金额：<span id="t_totalprice"></span></div></td>
    		</tr>
    		<tr>
    			<td><div>积分：<span id="t_integral"></span></div></td>
    		</tr>
    	</table>
    	
    	
    	
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<div>应收金额：<span id="t_receivables"></span></div>
    	<div>实收金额：<span id="t_realpay"></span></div>
    	<div>找款：<span id="t_change"></span></div>
    	<div>交易时间：<span id="t_time"></span></div>
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<div>欢迎光临Dainty-Bits！</div>
    	<div>本小票为退货凭据，请保留小票以便查核</div>
    	<div>发票于当月内开具，过期无效</div>
    	<div>多谢惠顾，欢迎再次光临</div>
    	<br />
    </div>
    
    <!-- 选择会员窗口 -->
    <div id="select_customer">
    	<div style="height:430px;width:1000px;margin-top:0;">
    		<table id="select_customer_grid"></table>
    	</div>
    </div>
    
    <!-- 选择会员datagrid的工具栏 -->
	<fieldset id="customer_tb">
		<div style="margin:4px;float:left;">
			<!-- 搜索框 -->
	        <div id="search"></div>
			<div id="options">
				<div data-options="name:'id',iconCls:'icon-ok'">会员编号</div>
				<div data-options="name:'name',iconCls:'icon-ok'">姓名/姓名关键字</div>
				<div data-options="name:'cellphone',iconCls:'icon-ok'">联系电话</div>
			</div>
		</div>
		<!-- 通过日期区间搜索 -->
		<div style="float:left;margin:4px;">
			<form id="search_date" method="post" >
			注册日期  从: <input class="easyui-datebox" style="width:120px" name="firstDate" id="firstDate" />
			到: <input class="easyui-datebox" style="width:120px" name="lastDate" id="lastDate" />
			<a href="javascript:void(0)" id="btn_search_date" class="easyui-linkbutton" iconCls="icon-search">搜索</a>
		</form>
		</div>
		
	</fieldset> 
    
    <!-- 选择商品窗口的表格 -->
    <div id="select_goods">
    	<div style="height:400px;width:800px;margin-top:0;">
    		<table id="select_goods_grid"></table>
    	</div>
    </div>
    
    <!-- 添加会员的对话框 -->
	<div id="customer_add_dialog" style="padding-right:60px;width:400px;height: 320px;text-align:right">	
		<form id="form" method="post">
			<div style="margin:10px;">
				<span>姓名：</span> 
				<input name="name" class="easyui-textbox" style="width:200px;"
					data-options="required:true" />
			</div>
			<div style="margin:10px;">
				<span>性别：</span> 
				<select name="gender" id="add_gender" style="width:200px;"
					data-options="editable:false,panelHeight:'auto'">
					<option>男</option>
					<option>女</option>
				</select>
			</div>
			<div style="margin:10px;">
				<span>折扣：</span> 
				<input id="add_discount" name="discount" class="easyui-textbox" style="width:200px;"
					data-options="editable:false,value:'0.95'" />
			</div>
			<div style="margin:10px;">
				<span>会员积分：</span> 
				<input name="integral" id="add_integral" class="easyui-textbox" style="width:200px;" 
				data-options="editable:false,value:'0'"/>
			</div>
			<div style="margin:10px;">
				<span>联系电话：</span> 
				<input name="cellphone" class="easyui-textbox" style="width:200px;"
					data-options="required:true" />
			</div>
			<div style="margin:10px;">
				<span>地址：</span> 
				<input name="address" class="easyui-textbox"
					data-options="required:true,multiline:true"
					style="width:200px;height:60px" />
			</div>
		</form>
	</div>
	
	<!-- 商品datagrid的工具栏 -->
	<fieldset id="tb_goods">
		<div style="margin:4px;float:left;">
			<!-- 搜索框 -->
			<div id="goods_search"></div>
			<div id="goods_options">
				<div data-options="name:'barcode',iconCls:'icon-ok'">商品条码</div>
				<div data-options="name:'name',iconCls:'icon-ok'">商品名/商品名关键字</div>
				<div data-options="name:'typeName',iconCls:'icon-ok'">商品类型/商品类型关键字</div>
			</div>
		</div>
	</fieldset>
	
	<!-- 提取账单 -->
    <div id="get_suspend_order">
    	<div style="height:400px;width:800px;margin-top:0;">
    		<table id="suspend_grid"></table>
    	</div>
    </div>
    
    
    
  </body>
</html>
