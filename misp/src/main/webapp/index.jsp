<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>Dainty-Bits</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/gray/easyui.css">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/icon.css">
<script src="jquery-easyui-1.4.2/jquery.min.js"></script>
<script src="jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
<script src="jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>
<script src="js/index.js"></script>
<style type="text/css">
</style>
</head>
<body>
	<div class="easyui-layout" id="main">
		<div id="head" data-options="region:'north'" style="height:60px;background-color:#666666">
			<div style="color:#fff;font-size:36px;font-weight:bold;padding:6px;margin-left: 10px;font-family:'Lucida Sans Unicode','Verdana';">
				Dainty-Bits
				<!-- <span style="font-size:14px;font-weight:normal;">It's a dream for bread lovers !</span> -->
			</div>
			<div style="text-align: right;color:#fff;font-size:20px;padding:8px">
				<a href="logout.action">请先退出</a>
			</div>
		</div>
		<div id="foot" data-options="region:'south'" style="height:30px;text-align: center;background: #D2E0F2">CopyRight:gaoshenmegui</div>
		<div id="nav" data-options="region:'west',split:true" style="width:200px" title="导航">
			<div id="navMenu">
				<!-- <div title="系统管理" data-options="iconCls:'icon-reload'" style="overflow:auto;padding:10px;"></div> -->

				<div title="基础管理" data-options="iconCls:'icon-edit',selected:true" style="padding:10px;">
					<ul id="tree"></ul>
				</div>
				<div title="前台业务" data-options="iconCls:'icon-sum',selected:true" style="padding:10px;">
					<ul id="checkout_tree"></ul>
				</div>
			</div>
		</div>
		<div id="mainPanle" data-options="region:'center'">
			<div id="tabs">
				<div title="首页" data-options="iconCls:'icon-tip'">
					<iframe src="about.jsp" style="border: 0;padding:0;margin:0; width: 100%; height: 98%;" frameBorder="0"></iframe>
				</div>
			</div>
		</div>
	</div>

	<!-- 选项卡的右键菜单 -->
	<div id="tabsMenu" style="width:150px;">
		<div data-options="name:'close'">关闭</div>
		<div></div>
		<div data-options="name:'closeall'">全部关闭</div>
		<div data-options="name:'closeother'">关闭其它</div>
	</div>

	<!-- 登录 -->
	<div id="login_dialog" style="padding:15px;">
		<form action="" method="post">
			<div style="margin-top:5px;">
				账 号： <input class="easyui-textbox" id="username" data-options="iconCls:'icon-man',prompt:'Username'" style="width:200px;height:30px;">
			</div>
			<div style="margin-top:5px;">
				密 码： <input class="easyui-textbox" id="password" data-options="iconCls:'icon-lock',prompt:'Password'" style="width:200px;height:30px;">
			</div>
			<div style="margin-top:5px;">
				<label>验证码 </label> <input name="j_code" class="easyui-textbox" id="captcha" maxlength="8" class="form-control"style="width:200px;height:30px;" /> <br />
				 <img src="kaptcha.jpg" id="kaptchaImage" style="margin-bottom: -10px;margin-left: 40px;margin-top: 5px;" /> <a href="#" onclick="changeCode()">看不清?换一张</a>
			</div>
			<div id="error" style="margin-top:5px;">
			</div>
		</form>
	</div>
	<script type="text/javascript">
	$(function(){  //生成验证码         
	    $('#kaptchaImage').click(function () {  
	    $(this).hide().attr('src', 'kaptcha.jpg').fadeIn(); });      
	});   
	 
	window.onbeforeunload = function(){  
	    //关闭窗口时自动退出  
	    if(event.clientX>360&&event.clientY<0||event.altKey){     
	        alert(parent.document.location);  
	    }  
	};  
	             
	function changeCode() {  //刷新
	    $('#kaptchaImage').hide().attr('src', 'kaptcha.jpg?' + Math.floor(Math.random()*100) ).fadeIn();  
	    //window.event? window.event.cancelBubble = true : e.stopPropagation();
	}  
	</script>
</body>
</html>