<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'user.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet"
	href="jquery-easyui-1.4.2/themes/gray/easyui.css">
	<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/icon.css">
	<script src="jquery-easyui-1.4.2/jquery.min.js"></script>
	<script src="jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
	<script src="jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>
	<script src="js/role.js"></script>

  </head>
  <body>
	<div style="width:100%;height:100%;padding:0px;overflow:hidden">
		
		<!-- 表格 -->
		<table id="role_grid"></table>
			
		<!-- 添加角色类型的对话框 -->
		<div id="role_dialog" style="width:450px;height: 300px;text-align:right;padding-right:40px;" ;>	
			<form id="role_form" method="post">
				<div style="margin:10px;" id="role_id_div">
					<label>角色编号：</label> 
					<input id="role_id" name="role.id"  class="easyui-textbox"
						data-options="required:false,editable:false" style="width:280px;" />
				</div>
				<div style="margin:10px;">
					<label>角色名称：</label> 
					<input name="role.name" class="easyui-textbox" style="width:280px;"
						data-options="required:true" />
				</div>
				<div style="margin:10px;">
					<label>角色代码：</label> 
					<input name="role.roleCode" class="easyui-textbox" style="width:280px;"
						data-options="required:true" />
				</div>
				<div style="margin:10px;">
					<label>描述：</label> 
					<input name="role.description" class="easyui-textbox" data-options="multiline:true" style="width:280px;height:100px">
					<!-- <textarea name="role.description" /> -->
				</div>
			</form>
		</div>
		<!-- datagrid的工具栏 -->
		<div id="tb_role" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">
			    <shiro:hasPermission name="sys:role:add">
				<div id="tb_role_add" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</div>
				</shiro:hasPermission>
				<shiro:hasPermission name="sys:role:update">
				<div id="tb_role_edit" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</div>
				</shiro:hasPermission>
				<shiro:hasPermission name="sys:role:delete">
				<div id="tb_role_remove" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</div>
				</shiro:hasPermission>
				
				<div id="tb_role_reload" class="easyui-linkbutton" iconCls="icon-reload" plain="true">刷新</div>
				
			</div>
		</div>
	</div>

  </body>
</html>
