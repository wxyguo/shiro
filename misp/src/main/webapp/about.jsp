<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<meta charset="utf-8" />
<div style="color:#666">
	<h2>欢迎使用！</h2>
	<div>
	<h4>会员规则：</h4>
	<ul>
		<li>一次性购买本店商品超过35元可申请成为会员</li>
		<li>会员折扣：9.5折</li>
	</ul>
	<h4>积分规则：</h4>
	<ul>
		<li>凡是会员均可获得本店积分</li>
		<li>购买本店商品每1元可获得1积分，不足1元部分不计</li>
		<li>满100积分可兑换本店10元以下的任意商品，以此类推</li>
		<li>积分限期为每个季度第一天归0</li>
		<li>购物小票上可查看总积分统计</li>
	</ul>
	<h4>退货规则：</h4>
	<div style="padding-left:25px;">如购买本店商品出现以下情况允许退货：</div>
	<ul>
		<li>商品在保质期内但存在质量问题</li>
		<li>商品包装存在异常问题</li>
		<li>销售过期商品</li>
	</ul>
	</div>

</div>
<br/>
