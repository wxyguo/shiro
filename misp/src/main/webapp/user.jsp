<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'user.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/gray/easyui.css">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/icon.css">
<script src="jquery-easyui-1.4.2/jquery.min.js"></script>
<script src="jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
<script src="jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>
<script src="js/user.js"></script>

</head>
<body>
	<div style="width:100%;height:100%;padding:0px;overflow:hidden">

		<!-- 表格 -->
		<table id="user_grid"></table>

		<!-- 添加系统新用户的对话框 -->
		<div id="user_dialog" style="width:400px;height: 350px;text-align:right;padding-right:60px;">
			<form id="user_form" method="post">
				<div style="margin:10px;">
					<label>用户编号：</label> <input name="id" class="easyui-textbox" style="width:200px;" data-options="required:false" disabled />
				</div>
				<div style="margin:10px;">
					<label>姓名：</label> <input name="user.name" class="easyui-textbox" style="width:200px;" data-options="required:true" />
				</div>
				<div style="margin:10px;">
					<label>性别：</label> <select name="user.gender" id="user_gender" style="width:200px;">
						<option>男</option>
						<option>女</option>
					</select>
				</div>
				<div style="margin:10px;">
					<span>用户类型：</span> <input name="roleName" class="easyui-combobox" id="user_role" style="width:200px;" />

				</div>
				<div style="margin:10px;" id="hide">
					<input name="roleId" class="easyui-textbox" id="roleId" style="width:200px;" />
				</div>
				<div style="margin:10px;">
					<span>生日：</span> <input class="easyui-datebox" style="width:200px" name="user.birthday" id="birthday" />
				</div>
				<div style="margin:10px;">
					<span>email：</span> <input name="user.email" class="easyui-textbox" style="width:200px;" data-options="required:true" />
				</div>
				<div style="margin:10px;">
					<label>联系电话：</label> <input name="user.cellphone" class="easyui-textbox" style="width:200px;" data-options="required:true" />
				</div>
			</form>
		</div>


		<!-- datagrid的工具栏 -->
		<div id="tb_user" style="padding:5px;height:auto">
			<div style="margin-bottom:5px">
			    <shiro:hasPermission name="sys:user:add">
				<div id="tb_user_add" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</div>
				</shiro:hasPermission>
				<shiro:hasPermission name="sys:user:update">
				<div id="tb_user_edit" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</div>
				</shiro:hasPermission>
				<shiro:hasPermission name="sys:user:delete">
				<div id="tb_user_remove" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</div>
				</shiro:hasPermission>
				
				<div id="tb_user_reload" class="easyui-linkbutton" iconCls="icon-reload" plain="true">刷新</div>
				<!-- 搜索框 -->
				<div id="user_search"></div>
				<div id="options">
					<div data-options="name:'id',iconCls:'icon-ok'">用户编号</div>
					<div data-options="name:'name',iconCls:'icon-ok'">姓名/姓名关键字</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
