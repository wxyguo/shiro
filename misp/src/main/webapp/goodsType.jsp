<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>商品类型</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/gray/easyui.css">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/icon.css">
<link rel="stylesheet" href="css/style.css">
<script src="jquery-easyui-1.4.2/jquery.min.js"></script>
<script src="jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
<script src="jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>

<script src="js/goodsType.js"></script>


</head>
<body>
	<div style="width:100%;height:100%;padding:0px;overflow:hidden;">
		<table id="goods_type_grid"></table>
		<!-- 添加会员的对话框 -->
		<div id="type_add_dialog">
		<form id="type_form" method="post">
		<table id="table" style="width:480px;height: 200px;">
			<tr>
				<td style="text-align:right;padding-right:20px">有无上级商品类型</td>
				<td><label>有</label><input id="radyes" type="radio" name="property" value="yes" /> 
					<label>无</label><input id="radno" type="radio" name="property" value="no"checked="checked" />
				</td>
			</tr>
			<tr>
				<td style="text-align:right;padding-right:20px" >所属的上级商品类型</td>
				<td><input id="pname" name="pname" style="width:155px;" class="easyui-combotree" data-options="required:false"/></td>
			</tr>
			<tr>
				<td style="text-align:right;padding-right:20px">填入商品类型名字</td>
				<td><input id="name" name="name" class="easyui-textbox" data-options="required:false"/></td>
			</tr>
			<tr>
			    <td id="hide"><input id="pbid" name="pid"  style="width:150px;" class="easyui-textbox" data-options="required:false">
			    </td>
			</tr>
		</table>
		</form>
		</div>
		
		<div id="type_edit_dialog">
			<form id="type_edit_form" method="post">
				<table style="width:380px;height: 150px;">
					<tr>
						<td style="text-align:right;padding-right:20px">修改商品类型</td>
						<td><input id="editname" name="name" class="easyui-textbox" data-options="required:false" />
						</td>
					</tr>

				</table>
			</form>
		</div>
	</div>
</body>
</html>