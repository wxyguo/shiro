<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
	<title>综合演示用例</title>
</head>

<body>
	<h1>帐号管理</h1>
			
		<table style="width: 100%" border="1">
			<thead><tr>
				<th align="left">姓名</th>
				<th align="left">描述</th>
				<th align="left">操作</th>
			</tr></thead>
			<tbody>
			
			<s:iterator value="userList">
				<tr>
					<td>${name}&nbsp;</td>
					<td>${content}&nbsp;</td>
					<td>
					
						<!-- 这个name就是  ShiroDbRealm 类中 info.addRole("admin"); 目前只能一个 -->
						<shiro:hasRole name="admin">
							here
						</shiro:hasRole>
					
						<!-- 这个name就是  ShiroDbRealm 类中  info.addStringPermission("user:edit");  -->
						<shiro:hasPermission name="user:edit">
							<a href="about.jsp">修改</a>
						</shiro:hasPermission>
						
						
					</td>
				</tr>
			</s:iterator>
			</tbody>		
		</table>
		
		<h1><a href="logout.action">请先退出</a></h1>
</body>
</html>
