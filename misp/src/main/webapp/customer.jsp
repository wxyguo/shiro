<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
<title>会员</title>
<meta charset="utf-8" />
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/gray/easyui.css">
<link rel="stylesheet" href="jquery-easyui-1.4.2/themes/icon.css">
<script src="jquery-easyui-1.4.2/jquery.min.js"></script>
<script src="jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
<script src="jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>
<script src="jquery-validation-1.11.1/jquery.validate.js"></script>
<script src="js/index.js" charset="utf-8"></script>
<script src="js/customer.js"></script>
<style type="text/css">
	.datagrid-row {
        height: 32px;
        font-size:16px;
    }
</style>

</head>

<body>
	<div style="width:100%;height:100%;padding:0px;overflow:hidden">
	
		<!-- 表格 -->
		<table id="customer_grid"></table>
			
		<!-- 添加会员的对话框 -->
		<div id="customer_add_dialog" style="padding-right:60px;width:400px;height: 320px;text-align:right">	
			<form id="form" method="post">
				<div style="margin:10px;">
					<span>姓名：</span> 
					<input name="name" class="easyui-textbox" style="width:200px;"
						data-options="required:true" />
				</div>
				<div style="margin:10px;">
					<span>性别：</span> 
					<select name="gender" id="add_gender" style="width:200px;"
						data-options="editable:false,panelHeight:'auto'">
						<option>男</option>
						<option>女</option>
					</select>
				</div>
				<div style="margin:10px;">
					<span>折扣：</span> 
					<input id="add_discount" name="discount" class="easyui-textbox" style="width:200px;"
						data-options="editable:false,value:'0.95'" />
				</div>
				<div style="margin:10px;">
					<span>会员积分：</span> 
					<input name="integral" id="add_integral" class="easyui-textbox" style="width:200px;" 
					data-options="editable:false,value:'0'"/>
				</div>
				<div style="margin:10px;">
					<span>联系电话：</span> 
					<input name="cellphone" class="easyui-textbox" style="width:200px;"
						data-options="required:true" />
				</div>
				<div style="margin:10px;">
					<span>地址：</span> 
					<input name="address" class="easyui-textbox"
						data-options="required:true,multiline:true"
						style="width:200px;height:60px" />
				</div>
			</form>
		</div>
		
	
	<!-- 修改会员的对话框 -->
	<div id="customer_edit_dialog" style="width:530px;height: 420px;">
		<form id="edit_form" method="post">
			<div style="margin:10px;">
				<label>会员编号：</label> 
				<input name="id" style="width:200px;height:30px"
					data-options="required:false" disabled />
			</div>
			<div style="margin:10px;">
				<label>姓名：</label> 
				<input name="name" style="width:200px;height:30px"
					data-options="required:true" />
			</div>
			<div style="margin:10px;">
				<label>性别：</label> 
				<select name="gender" style="width:200px;height:30px"
					data-options="required:true">
					<option value="男">男</option>
					<option value="女">女</option>
				</select>
			</div>
			<div style="margin:10px;">
				<label>折扣：</label> 
				<input name="discount" style="width:200px;height:30px"
					data-options="required:true" />
			</div>
			<div style="margin:10px;">
				<label>会员积分：</label> 
				<input type="text" name="integral" style="width:200px;height:30px"
					data-options="required:true" value="0" />
			</div>
			<div style="margin:10px;">
				<label>联系电话：</label> 
				<input name="cellphone" style="width:200px;height:30px"
					data-options="required:true" />
			</div>
			<div style="margin:10px;">
				<label>地址：</label> 
				<input name="address"
					data-options="required:true" style="width:300px;height:30px;" />
			</div>
		</form>
	</div>
		
	<!-- datagrid的工具栏 -->
	<div id="tb" style="padding:5px;height:auto">
		<div style="margin-bottom:5px">
			<div id="tb_add" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</div>
            <!-- <div class="datagrid-btn-separator"></div> -->
			<div id="tb_edit" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</div>
			<div id="tb_remove" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</div>
			<div id="tb_reload" class="easyui-linkbutton" iconCls="icon-reload" plain="true">刷新</div>
			<div id="tb_save" class="easyui-linkbutton" iconCls="icon-save" plain="true">保存</div>
			<!-- 搜索框 -->
	        <div id="search"></div>
			<div id="options">
				<div data-options="name:'id',iconCls:'icon-ok'">会员编号</div>
				<div data-options="name:'name',iconCls:'icon-ok'">姓名/姓名关键字</div>
				<div data-options="name:'cellphone',iconCls:'icon-ok'">联系电话</div>
			</div>
		</div>
		<!-- 通过日期区间搜索 -->
		<form id="search_date" method="post">
			注册日期  从: <input class="easyui-datebox" style="width:120px" name="firstDate" id="firstDate" />
			到: <input class="easyui-datebox" style="width:120px" name="lastDate" id="lastDate" />
			<a href="javascript:void(0)" id="btn_search_date" class="easyui-linkbutton" iconCls="icon-search">搜索</a>
		</form>
	</div> 
	
	</div>
</body>
</html>